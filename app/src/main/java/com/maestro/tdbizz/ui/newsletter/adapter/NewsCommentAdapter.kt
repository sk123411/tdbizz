package com.maestro.tdbizz.ui.newsletter.adapter

import android.content.DialogInterface
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.ProgressBar
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.LinearLayoutCompat
import androidx.core.view.isVisible
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.observe
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.androidnetworking.AndroidNetworking
import com.androidnetworking.error.ANError
import com.androidnetworking.interfaces.ParsedRequestListener
import com.maestro.tdbizz.R
import com.maestro.tdbizz.databinding.CommentItemLytBinding
import com.maestro.tdbizz.helper.Constant
import com.maestro.tdbizz.ui.newsletter.*
import com.maestro.tdbizz.ui.newsletter.adapter.`interface`.NestedToParentChangesInterface
import com.vanniktech.emoji.EmojiPopup
import java.util.*


class NewsCommentAdapter(val list: List<NewsLikeCommentAddedResponse>,
                         val newsViewModel: NewsViewModel,
                         val newsFeedFragment: NewsFragment,
                        val activity:FragmentActivity) :
    RecyclerView.Adapter<NewsCommentAdapter.MyViewholder>() {
    class MyViewholder(view: View) : RecyclerView.ViewHolder(view) {

        val commentItemLytBinding = CommentItemLytBinding.bind(view)
        fun bindData(
            data: NewsLikeCommentAddedResponse, newsViewModel:
            NewsViewModel, newsFeedFragment: NewsFragment, activity: FragmentActivity
        ) {


            if (data.userId.equals(Constant.getUserID(context =
                commentItemLytBinding.root.context).id)){


                val lp =
                    LinearLayoutCompat.LayoutParams(
                        LinearLayout.LayoutParams.WRAP_CONTENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT
                    )
                lp.weight = 1.0f;
                lp.gravity = Gravity.TOP;

                commentItemLytBinding.commentRootLayout.layoutParams = lp
                commentItemLytBinding.deleteButton.visibility = View.VISIBLE

            } else {
                commentItemLytBinding.deleteButton.visibility = View.GONE


            }
            commentItemLytBinding.userName.text = data.id
            commentItemLytBinding.userComment.setText(data.comment)










//            commentItemLytBinding.userComment.text = itemView.resources.getString(R.string.emoji)

            //  commentItemLytBinding.userLabel.text = data.userId.substring(0,1)


            commentItemLytBinding.replyCommentButton.setOnClickListener {
                if (!commentItemLytBinding.replyCommentLayout.isVisible) {
                    commentItemLytBinding.replyCommentLayout.visibility = View.VISIBLE
                } else {

                }
            }


            if (commentItemLytBinding.deleteButton.isVisible){

                commentItemLytBinding.deleteButton.setOnClickListener {


                    val alertDialog = AlertDialog.Builder(activity)
                    alertDialog.setTitle("Delete comment!")
                    alertDialog.setMessage("Do you want to delete this comment?")
                    alertDialog.setPositiveButton("Yes", object: DialogInterface.OnClickListener{
                        override fun onClick(dialog: DialogInterface?, which: Int) {
                            newsFeedFragment.activity?.findViewById<ProgressBar>(R.id.loadingBar)?.isVisible =
                                true

                            AndroidNetworking.post("https://ruparnatechnology.com/TDbizz/api/process.php?action=delete_comment")
                                .addBodyParameter("user_id", Constant.getUserID(itemView.context).id)
                                .addBodyParameter("id",data.id)
                                .build().getAsObject(NestedCommentResponse::class.java, object :ParsedRequestListener<NestedCommentResponse>{
                                    override fun onResponse(response: NestedCommentResponse?) {
                                        newsFeedFragment.activity?.findViewById<ProgressBar>(R.id.loadingBar)?.isVisible =
                                            false

                                        newsViewModel.getCommentsForBLog(
                                            Constant.getUserID(itemView.context).id,
                                            data.blogId
                                        )

                                    }

                                    override fun onError(anError: ANError?) {
                                    }
                                })

                        }

                    }).setNegativeButton("No", object : DialogInterface.OnClickListener{
                        override fun onClick(dialog: DialogInterface?, which: Int) {
                            dialog!!.dismiss()
                        }

                    }).show()



                }

            }



            val emojiPopup = EmojiPopup.Builder.fromRootView(itemView).build(commentItemLytBinding.messageEdit);
            commentItemLytBinding.messageEdit.setOnClickListener {

                emojiPopup.toggle();


            }



            commentItemLytBinding.sendMessageButtton.setOnClickListener {

                if (commentItemLytBinding.replyCommentLayout.isVisible){


                    if (!commentItemLytBinding.messageEdit.text.toString().isEmpty()) {
                        newsFeedFragment.activity?.findViewById<ProgressBar>(R.id.loadingBar)?.isVisible =
                            true

                        newsViewModel.addReplyOnComment(
                            Constant.getUserID(itemView.context).id,
                            data.id,
                            commentItemLytBinding.messageEdit.text.toString()
                        )

                        commentItemLytBinding.messageEdit.setText("")
                        commentItemLytBinding.replyCommentLayout.visibility = View.GONE
                    }else {
                        Toast.makeText(itemView.context, "Please! type your comment",Toast.LENGTH_LONG).show()
                    }

                }

            }

            getNestedComments(newsViewModel,newsFeedFragment,data.id,data)






            NewsCommentAdapter.refreshNestedComments = object : RefreshNestedComments{
                override fun refresh(boolean: Boolean, commentId:String) {
                    newsFeedFragment.activity?.findViewById<ProgressBar>(R.id.loadingBar)?.isVisible =
                        true
                    getNestedComments(newsViewModel,newsFeedFragment,commentId,data)


                }

            }






            newsViewModel.newsEvent.observe(
                newsFeedFragment.viewLifecycleOwner,
                androidx.lifecycle.Observer {

                    when (it) {
                        is NewsViewModel.NewsEvent.Loading -> {
                            newsFeedFragment.activity?.findViewById<ProgressBar>(R.id.loadingBar)?.isVisible =
                                true

                        }
                        is NewsViewModel.NewsEvent.SuccessAddReplyComment -> {

                            Log.d("TESTTTTTTTTTT", ":::" + "cool")
                            newsFeedFragment.activity?.findViewById<ProgressBar>(R.id.loadingBar)?.isVisible = false
                            newsViewModel.getCommentsForBLog(
                                Constant.getUserID(itemView.context).id,
                                data.blogId
                            )
                            /*
                            AndroidNetworking.post("https://ruparnatechnology.com/TDbizz/api/process.php?action=show_comment_oncomment")
                                .addBodyParameter("user_id", Constant.getUserID(itemView.context).id)
                                .addBodyParameter("comment_id", data.id)
                                .build().getAsObjectList(
                                    NestedCommentResponse::class.java,
                                    object : ParsedRequestListener<List<NestedCommentResponse>> {
                                        override fun onResponse(response: List<NestedCommentResponse>?) {
                                            if (response!!.isNotEmpty()) {

                                                commentItemLytBinding.nestedCommentList.apply {
                                                    layoutManager = LinearLayoutManager(context)
                                                    adapter = NestedCommentAdapter(
                                                        data.id,
                                                        response!!.toMutableList(),
                                                        newsViewModel,
                                                        newsFeedFragment.requireActivity()
                                                    )
                                                }
                                            } else {
                                                commentItemLytBinding.nestedCommentList.visibility = View.GONE

                                            }
                                        }

                                        override fun onError(anError: ANError?) {
                                        }

                                    })
*/

                        }

                        is NewsViewModel.NewsEvent.SuccessDeleteComment -> {

                            newsViewModel.getCommentsForBLog(
                                Constant.getUserID(itemView.context).id,data.blogId
                            )
                        }
                    }


                }
            )
        }

        private fun getNestedComments(
            newsViewModel: NewsViewModel,
            newsFeedFragment: NewsFragment,
            commentId:String,
            newsResponseItem: NewsLikeCommentAddedResponse) {

            newsFeedFragment.activity?.findViewById<ProgressBar>(R.id.loadingBar)?.isVisible =
                true

            Log.d("uuuuuuuuuuu", "::"+Constant.getUserID(itemView.context).id)
            Log.d("uuuuuuuuuuuCCCCCCCCC", "::"+commentId)

            AndroidNetworking.post("https://ruparnatechnology.com/TDbizz/api/process.php?action=show_comment_oncomment")
                .addBodyParameter("user_id", Constant.getUserID(itemView.context).id)
                .addBodyParameter("comment_id", commentId)
                .build().getAsObjectList(
                    NestedCommentResponse::class.java,
                    object : ParsedRequestListener<List<NestedCommentResponse>> {
                        override fun onResponse(response: List<NestedCommentResponse>?) {
                            if (response!!.isNotEmpty()) {


                                    commentItemLytBinding.nestedCommentList.apply {
                                        layoutManager = LinearLayoutManager(context)
                                        adapter = NestedCommentAdapter(
                                            newsFeedFragment,
                                            newsResponseItem.id,
                                            response!!.toMutableList(),
                                            response!!.toMutableList().size,
                                            newsFeedFragment.requireActivity()
                                        )

                                    }



                            } else {
                                commentItemLytBinding.nestedCommentList.visibility = View.GONE

                            }
                        }

                        override fun onError(anError: ANError?) {
                        }

                    })
        }


    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewholder {

        val binding = CommentItemLytBinding.inflate(LayoutInflater.from(parent.context))


        return MyViewholder(binding.root)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: MyViewholder, position: Int) {

        holder.bindData(list.get(position),newsViewModel,newsFeedFragment,activity)




    }
    companion object {

        var refreshNestedComments:RefreshNestedComments?=null
        var nestedToParsedRequestListener:NestedToParentChangesInterface?=null

    }

}