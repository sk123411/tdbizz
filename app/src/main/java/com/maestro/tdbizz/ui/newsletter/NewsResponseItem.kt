package com.maestro.tdbizz.ui.newsletter


import com.google.gson.annotations.SerializedName

data class NewsResponseItem(
    @SerializedName("author")
    val author: String,
    @SerializedName("category_id")
    val categoryId: String,
    @SerializedName("description")
    val description: String,
    @SerializedName("id")
    val id: String,
    @SerializedName("image_video")
    val image_video: String,
    @SerializedName("image1")
    val image1: String,
    @SerializedName("image2")
    val image2: String,
    @SerializedName("like_status")
    val likeStatus: Int,
    @SerializedName("strtotime")
    val strtotime: String,
    @SerializedName("title")
    val title: String,
    @SerializedName("comment_count")
    val comment_count: Int,
    @SerializedName("like_count")
    val like_count: Int,
    @SerializedName("status")
    val status: String,
    @SerializedName("view_count")
    val view_count: String,
    @SerializedName("share_count")
    val share_count: String


)