package com.maestro.tdbizz.ui.product

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.maestro.tdbizz.R
import com.maestro.tdbizz.databinding.FragmentProductListBinding
import com.maestro.tdbizz.ui.product.data.ProductViewModel

class ProductListFragment : Fragment() {

    lateinit var binding: FragmentProductListBinding

    private val productViewModel: ProductViewModel by viewModels()
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentProductListBinding.inflate(layoutInflater)
        // Inflate the layout for this fragment


        val id = arguments?.getString("id")

        if (id != null) {

            productViewModel.getProducts(id)

        }






        lifecycleScope.launchWhenStarted {


            productViewModel.productEvent.observe(viewLifecycleOwner, Observer {


                when (it) {


                    is ProductViewModel.ProductEvent.Loading -> {


                        activity?.findViewById<ProgressBar>(R.id.loadingBar)?.isVisible = true

                    }


                    is ProductViewModel.ProductEvent.SuccessProducts -> {

                        activity?.findViewById<ProgressBar>(R.id.loadingBar)?.isVisible = false

                        binding.productsList.apply {


                            layoutManager = LinearLayoutManager(
                                context,
                                LinearLayoutManager.HORIZONTAL, false
                            )
                            adapter =
                                ProductAdapter(it.products)
                        }


                    }

                }


            })


        }






        binding.productsList.apply {


            layoutManager = GridLayoutManager(context, 2)
            //   adapter = ProductAdapter(list = mutableListOf<Int>(1,2,4,5,6,7,8))
        }





        return binding.root
    }


}