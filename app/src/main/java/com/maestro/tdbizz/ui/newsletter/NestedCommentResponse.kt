package com.maestro.tdbizz.ui.newsletter

import androidx.annotation.Nullable
import com.google.gson.annotations.SerializedName

data class NestedCommentResponse(
 @SerializedName("comment_id")
 val comment_id: String,
 @SerializedName("id")
 val id: String,
 @SerializedName("result")
 val result: String,
 @SerializedName("strtotime")
 val strtotime: String,
 @SerializedName("user_id")
 val userId: String,
 @SerializedName("comment")
 @Nullable
 val comment:String?

 )
