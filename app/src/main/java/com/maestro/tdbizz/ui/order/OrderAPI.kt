package com.maestro.tdbizz.ui.order

import retrofit2.Response
import retrofit2.http.FieldMap
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST
import retrofit2.http.Query

interface OrderAPI {


    @POST("api/process.php")
    @FormUrlEncoded
    suspend fun addToCart(@Query("action") action:String?, @FieldMap data:HashMap<String,String>):Response<ProductToCartResponse>



    @POST("api/process.php")
    @FormUrlEncoded
    suspend fun getCart(@Query("action") action:String?, @FieldMap data:HashMap<String,String>):Response<List<CartListResponse>>

    @POST("api/process.php")
    @FormUrlEncoded
    suspend fun updateCart(@Query("action") action:String?, @FieldMap data:HashMap<String,String>):Response<ProductToCartResponse>



    @POST("api/process.php")
    @FormUrlEncoded
    suspend fun deleteProductFromCart(@Query("action") action:String?, @FieldMap data:HashMap<String,String>):Response<ProductToCartResponse>
}