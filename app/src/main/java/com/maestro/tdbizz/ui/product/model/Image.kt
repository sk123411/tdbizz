package com.maestro.tdbizz.ui.product.model


import com.google.gson.annotations.SerializedName

data class Image(
    @SerializedName("id")
    val id: String,
    @SerializedName("image")
    val image: String,
    @SerializedName("product_id")
    val productId: String
)