package com.maestro.tdbizz.ui.vendor

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.ProgressBar
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import com.github.dhaval2404.imagepicker.ImagePicker
import com.maestro.tdbizz.R
import com.maestro.tdbizz.databinding.FragmentVendorAddProductScreenBinding
import com.maestro.tdbizz.helper.Constant
import com.maestro.tdbizz.ui.category.CategoryResponseItem
import com.maestro.tdbizz.ui.category.SubCategoryResponseItem
import com.maestro.tdbizz.ui.home.data.HomeViewModel
import com.maestro.tdbizz.ui.vendor.data.VendorViewModel
import java.io.File

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [VendorAddProductScreen.newInstance] factory method to
 * create an instance of this fragment.
 */
class VendorAddProductScreen : Fragment() {


    lateinit var binding:FragmentVendorAddProductScreenBinding
    private val vendorViewModel:VendorViewModel by viewModels()

    var subcategoryID:String?="1"
    private val homeViewModel: HomeViewModel by viewModels()
    var nameCategory:MutableList<String>?= emptyList<String>().toMutableList()
    var nameSubcategory:MutableList<String>? = emptyList<String>().toMutableList()
    var categoryID:String?=""



    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentVendorAddProductScreenBinding.inflate(layoutInflater)
        // Inflate the layout for this fragment
        homeViewModel.getCategories()


        binding.profileImage.setOnClickListener {

            ImagePicker.with(this)
                .galleryOnly()
                .compress(1024)
                .start()


        }



        lifecycleScope.launchWhenStarted {


            homeViewModel.homeEvent.observe(viewLifecycleOwner, Observer {

                when(it){

                    is HomeViewModel.HomeEvent.SuccessCategories -> {
                        activity?.findViewById<ProgressBar>(R.id.loadingBar)?.isVisible = false


                        binding.categorySelectSpinner.apply {

                            adapter = ArrayAdapter<String>(requireContext(),android.R.layout.simple_list_item_1,it.categories.map(CategoryResponseItem::categoryName).toList())


                        }
                        binding.categorySelectSpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
                            override fun onNothingSelected(parent: AdapterView<*>?) {

                            }

                            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                                categoryID = it.categories.get(position).id

                                homeViewModel.getSubcategories(categoryID)
                            }

                        }

                    }

                    is HomeViewModel.HomeEvent.SuccessSubCategories -> {


                        activity?.findViewById<ProgressBar>(R.id.loadingBar)?.isVisible = false


                        binding.categorySubSelectSpinner.apply {

                            adapter = ArrayAdapter<String>(requireContext(),android.R.layout.simple_list_item_1,it.data.map(SubCategoryResponseItem::sub_category_name).toList())

                        }

                        binding.categorySubSelectSpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
                            override fun onNothingSelected(parent: AdapterView<*>?) {

                            }

                            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                                subcategoryID = it.data.get(position).id

                            }

                        }

                    }


                    is HomeViewModel.HomeEvent.Loading -> {
                        activity?.findViewById<ProgressBar>(R.id.loadingBar)?.isVisible = true

                    }

                }





            })

            vendorViewModel.vendorEvent.observe(viewLifecycleOwner, Observer {
                when(it){


                    is VendorViewModel.HomeEvent.SuccessProductAdded -> {
                        activity?.findViewById<ProgressBar>(R.id.loadingBar)?.isVisible = false

                        Toast.makeText(context, "Product added successfully",Toast.LENGTH_LONG).show()

                    }


                    is VendorViewModel.HomeEvent.Loading -> {

                        activity?.findViewById<ProgressBar>(R.id.loadingBar)?.isVisible = true





                    }


                }





            })




            vendorViewModel.imageFile.observe(viewLifecycleOwner, Observer {



                binding.productAddButton.setOnClickListener { v->


                    vendorViewModel.addProduct(it,binding.prTitle.text.toString(),binding.prDescription.text.toString(),
                    binding.prProductPrice.text.toString(),binding.prSize.text.toString(),binding.prColor.text.toString(),binding.prBrand.text.toString(),
                        categoryID,subcategoryID,Constant.getUserID(requireContext()).id)
                }




            })

        }




        return binding.root
    }
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            //Image Uri will not be null for RESULT_OK
            val fileUri = data?.data
            binding.profileImage.setImageURI(fileUri)
            //You can get File object from intent
            val file: File = ImagePicker.getFile(data)!!
            //You can also get File Path from intent
            vendorViewModel.sendImageFile(file)


            val filePath: String = ImagePicker.getFilePath(data)!!
        } else if (resultCode == ImagePicker.RESULT_ERROR) {
            Toast.makeText(context, ImagePicker.getError(data), Toast.LENGTH_SHORT).show()
        } else {
            Toast.makeText(context, "Task Cancelled", Toast.LENGTH_SHORT).show()
        }
    }
}