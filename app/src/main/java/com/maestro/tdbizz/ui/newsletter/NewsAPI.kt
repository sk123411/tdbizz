package com.maestro.tdbizz.ui.newsletter

import com.maestro.tdbizz.helper.Resource
import com.maestro.tdbizz.ui.newsletter.adapter.DeleteCommentResponse
import com.maestro.tdbizz.ui.newsletter.model.NewsAddViewResponse
import org.json.JSONObject
import retrofit2.Response
import retrofit2.http.FieldMap
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST
import retrofit2.http.Query

interface NewsAPI {

    @POST("api/process.php")
    @FormUrlEncoded
    suspend fun showBlog(@Query("action") action:String,@FieldMap data:HashMap<String,String>):Response<NewsResponse>

    @POST("api/process.php")
    @FormUrlEncoded
    suspend fun addCommentOnBlog(@Query("action") action: String, @FieldMap data:HashMap<String,String>):Response<NewsLikeCommentAddedResponse>

    @POST("api/process.php")
    @FormUrlEncoded
    suspend fun addLikeOnBlog(@Query("action") action: String, @FieldMap data:HashMap<String,String>):Response<NewsLikeCommentAddedResponse>


    @POST("api/process.php")
    @FormUrlEncoded
    suspend fun getCommentsForBLog(@Query("action") action: String, @FieldMap data:HashMap<String,String>):Response<List<NewsLikeCommentAddedResponse>>

    @POST("api/process.php")
    @FormUrlEncoded
    suspend fun subscribeForBlog(@Query("action") action: String, @FieldMap data:HashMap<String,String>):Response<SubscribeResponse>

    @POST("api/process.php")
    @FormUrlEncoded
    suspend fun getSubscribeStatus(@Query("action") action: String, @FieldMap data:HashMap<String,String>):Response<SubscribeStatusResponse>


    @POST("api/process.php")
    @FormUrlEncoded
    suspend fun addReplyOnCommentOnBlog(@Query("action") action: String, @FieldMap data:HashMap<String,String>):Response<NestedCommentResponse>


    @POST("api/process.php")
    @FormUrlEncoded
    suspend fun showRepliesOnCommentOnBlog(@Query("action") action: String, @FieldMap data:HashMap<String,String>):Response<List<NestedCommentResponse>>

    @POST("api/process.php")
    @FormUrlEncoded
    suspend fun deleteCommentOnBlog(@Query("action") action: String, @FieldMap data:HashMap<String,String>):Response<DeleteCommentResponse>

    @POST("api/process.php")
    @FormUrlEncoded
    suspend fun addViewOnBlog(@Query("action") action: String,
                          @FieldMap data:HashMap<String,String>):Response<NewsAddViewResponse>


}