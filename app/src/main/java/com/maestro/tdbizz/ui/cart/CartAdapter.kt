package com.maestro.tdbizz.ui.cart

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.maestro.tdbizz.R
import com.maestro.tdbizz.databinding.CartItemBinding
import com.maestro.tdbizz.helper.Constant
import com.maestro.tdbizz.ui.order.CartListResponse
import com.maestro.tdbizz.ui.order.OrderViewModel
import com.maestro.tdbizz.ui.order.ProductToCartResponse
import com.squareup.picasso.Picasso

class CartAdapter constructor(val carts:MutableList<CartListResponse>, val orderViewModel: OrderViewModel):RecyclerView.Adapter<CartAdapter.MyViewHolder>(){
    class MyViewHolder(v:View):RecyclerView.ViewHolder(v) {


        val binding = CartItemBinding.bind(v)


        fun bindData(cartListResponse: CartListResponse,orderViewModel: OrderViewModel){



            binding.productTitle.text = cartListResponse.productName
            binding.productQuantity.text = cartListResponse.quantity+ "x"
            Picasso.get().load(cartListResponse.path + cartListResponse.image).placeholder(R.drawable.logo).into(binding.productImage)
            binding.productPrice.text = "Rs "+cartListResponse.price


            binding.increaseQuantityButton.setOnClickListener {

                var updatedQuantity = cartListResponse.quantity.toInt() + 1
                binding.productQuantity.text = updatedQuantity.toString() + "x"
                orderViewModel.updateCart(cartListResponse.productId,Constant.getUserID(it.context).id,updatedQuantity.toString())


            }

            binding.decreaseQuantityButton.setOnClickListener {

                var updatedQuantity = cartListResponse.quantity.toInt() - 1
                binding.productQuantity.text = updatedQuantity.toString() + "x"

                orderViewModel.updateCart(cartListResponse.productId,Constant.getUserID(it.context).id,updatedQuantity.toString())


            }


            binding.deleteButton.setOnClickListener {


                orderViewModel.deleteProductFromCart(cartListResponse.productId, Constant.getUserID(
                    it.context).id)
            }




        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {

        return MyViewHolder(CartItemBinding.inflate(LayoutInflater.from(parent.context)).root)
    }

    override fun getItemCount(): Int {

        return carts.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {


        holder.bindData(carts.get(position),orderViewModel)



    }

}