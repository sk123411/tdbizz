package com.maestro.tdbizz.ui.home.data

import com.maestro.tdbizz.helper.Resource
import com.maestro.tdbizz.ui.category.CategoryResponse
import com.maestro.tdbizz.ui.category.SubCategoryResponseItem
import com.maestro.tdbizz.ui.home.ProductResponse
import com.maestro.tdbizz.ui.home.ProductResponseItem
import com.maestro.tdbizz.ui.home.offer.OfferResponse
import retrofit2.Response
import retrofit2.http.*

interface HomeAPI {


    @GET("api/process.php")
    suspend fun getCategory(@Query("action") action: String?): Response<CategoryResponse>

    @POST("api/process.php")
    @FormUrlEncoded
    suspend fun getSubcategory(
        @Query("action") action: String?, @FieldMap
        data: HashMap<String, String>
    ): Response<List<SubCategoryResponseItem>>



    @GET("api/process.php")
    suspend fun getProducts(@Query("action") action: String?): Response<ProductResponse>



    @GET("api/process.php")
    suspend fun getFeatureProducts(@Query("action") action: String?): Resource<List<ProductResponseItem>>


    @POST("api/process.php")
    @FormUrlEncoded
    suspend fun getProfile(
        @Query("action") action: String?, @FieldMap
        data: HashMap<String, String>
    ): Response<ProfileResponse>

    @GET("api/process.php")
    suspend fun getOffers(
        @Query("action") action: String?
    ): Response<OfferResponse>



    @POST("api/process.php")
    @FormUrlEncoded
    suspend fun updateProfile(
        @Query("action") action: String?, @FieldMap
        data: HashMap<String, String>
    ): Response<ProfileResponse>





}