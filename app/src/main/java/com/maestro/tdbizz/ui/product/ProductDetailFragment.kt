package com.maestro.tdbizz.ui.product

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.maestro.tdbizz.R
import com.maestro.tdbizz.databinding.FragmentProductDetailBinding
import com.maestro.tdbizz.helper.Constant
import com.maestro.tdbizz.ui.cart.CartAdapter
import com.maestro.tdbizz.ui.home.ProductResponseItem
import com.maestro.tdbizz.ui.order.OrderViewModel
import com.maestro.tdbizz.ui.product.data.ProductViewModel
import com.maestro.tdbizz.ui.product.model.Slider
import com.maestro.tdbizz.ui.product.slider.SliderAdapter
import com.smarteist.autoimageslider.IndicatorView.animation.type.IndicatorAnimationType
import com.smarteist.autoimageslider.SliderAnimations
import io.paperdb.Paper

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [ProductDetailFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class ProductDetailFragment : Fragment() {


    lateinit var binding:FragmentProductDetailBinding
    private val productViewModel: ProductViewModel by viewModels()
    private val orderViewModel: OrderViewModel by viewModels()

    private var isProductOnWishlist:Boolean? = false

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentProductDetailBinding.inflate(layoutInflater)
        // Inflate the layout for this fragment

        Paper.init(context)
        val product = Paper.book().read<ProductResponseItem>(Constant.PRODUCT_DETAILS)
        productViewModel.showWishlist(Constant.getUserID(requireContext()).id)
        orderViewModel.getCart(Constant.getUserID(requireContext()).id)


        binding.pdProductName.text = product.name
        binding.pDdescriptionText.text = product.desc
        binding.pdBrandText.text = product.brand
        binding.ppSize.text = product.size
        binding.priceText.text = "Rs "+product.price


        binding.addToCartButton.setOnClickListener {v->


            orderViewModel.addToCart(product.id,Constant.getUserID(requireContext()).id,"1")

        }
        try {


            val  sliderAdapter = SliderAdapter(requireContext())
            for (data in product.images){
                sliderAdapter.addItem(
                    Slider(
                        data.id,
                        data.product_id,
                        "${Constant.BASE_URL_IMAGE}${data.image}"
                    )
                )

            }

            binding.pdImageSlider.scrollTimeInMillis = 3000
            binding.pdImageSlider.isAutoCycle = true
            binding.pdImageSlider.setSliderAdapter(sliderAdapter)
            binding.pdImageSlider.setIndicatorAnimation(IndicatorAnimationType.WORM);
            binding.pdImageSlider.setSliderTransformAnimation(SliderAnimations.ANTICLOCKSPINTRANSFORMATION);
            binding.pdImageSlider.startAutoCycle();
        }catch (e:Exception){



        }













        binding.wishImage.setOnClickListener {

            if (!isProductOnWishlist!!) {
                binding.wishImage.setImageResource(R.drawable.heart)


                if (product.productID==null){

                    productViewModel.addProductToWishlist(
                        product.id,
                    Constant.getUserID(requireContext()).id
                    )


                }else {
                    productViewModel.addProductToWishlist(
                        product.productID,
                    Constant.getUserID(requireContext()).id
                    )



                }

                binding.wishImage.setImageResource(R.drawable.heart_full)

                isProductOnWishlist = true
            }else {


                binding.wishImage.setImageResource(R.drawable.heart_full)

                if (product.productID==null){

                    productViewModel.addProductToWishlist(
                        product.id,
                        Constant.getUserID(requireContext()).id
                    )


                }else {
                    productViewModel.addProductToWishlist(
                        product.productID,
                        Constant.getUserID(requireContext()).id
                    )


                }

                binding.wishImage.setImageResource(R.drawable.heart)

                isProductOnWishlist = false
            }


        }



        lifecycleScope.launchWhenStarted {

            productViewModel.productEvent.observe(viewLifecycleOwner, Observer {


                when (it) {


                    is ProductViewModel.ProductEvent.Loading -> {


                        activity?.findViewById<ProgressBar>(R.id.loadingBar)?.isVisible = true

                    }


                    is ProductViewModel.ProductEvent.SuccessProductAddedToWishlistt -> {

                        activity?.findViewById<ProgressBar>(R.id.loadingBar)?.isVisible = false


                        if (it.products.result.toLowerCase().contains("successfull")){

                            Toast.makeText(requireContext(),"Product wishlist updated",Toast.LENGTH_LONG).show()

                        }


                    }



                    is ProductViewModel.ProductEvent.SuccessProducts -> {

                        activity?.findViewById<ProgressBar>(R.id.loadingBar)?.isVisible = false


                        Log.d("TEEEEEEEEEEE", "::"+it.products)

                        for (p in it.products){

                            if (product.id.equals(p.productID)||p.productID.equals(product.productID)){
                                isProductOnWishlist = true


                            }
                        }

                        if (isProductOnWishlist!!){

                            binding.wishImage.setImageResource(R.drawable.heart_full)

                        }





                    }

                }


            })





        }


        lifecycleScope.launchWhenStarted {

            orderViewModel.orderEvent.observe(viewLifecycleOwner, Observer {



                when(it){



                    is OrderViewModel.OrderEvent.Success -> {

                        activity?.findViewById<ProgressBar>(R.id.loadingBar)?.isVisible = false
                        binding.addToCartButton.isEnabled = false
                        binding.addToCartButton.text = "Added to cart"
                        binding.addToCartButton.setTextColor(resources.getColor(
                            R.color.white))
                        binding.addToCartButton.setBackgroundColor(
                            resources.getColor(R.color.green_color))


                        Toast.makeText(requireContext(), "Added to cart", Toast.LENGTH_SHORT).show()
                    }

                    is OrderViewModel.OrderEvent.SuccessCartOrders -> {
                        activity?.findViewById<ProgressBar>(R.id.loadingBar)?.isVisible = false


                        for(item in it.productToCartResponses){

                            if ((product.id).equals(item.productId)){

                                binding.addToCartButton.isEnabled = false
                                binding.addToCartButton.text = "Added to cart"
                                binding.addToCartButton.setTextColor(resources.getColor(
                                R.color.white))
                                binding.addToCartButton.setBackgroundColor(
                                    resources.getColor(R.color.green_color))


                            }

                        }

                    }


                    is OrderViewModel.OrderEvent.Loading -> {

                        activity?.findViewById<ProgressBar>(R.id.loadingBar)?.isVisible = true


                    }
                }




            })
        }








        return binding.root
    }





}