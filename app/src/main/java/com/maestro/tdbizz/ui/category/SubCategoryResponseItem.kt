package com.maestro.tdbizz.ui.category


import com.google.gson.annotations.SerializedName

data class SubCategoryResponseItem(
    @SerializedName("sub_category_name")
    val sub_category_name: String,
    @SerializedName("id")
    val id: String,
    @SerializedName("cateogry_id")
    val cateogry_id: String,
    @SerializedName("image")
    val image: String)