package com.maestro.tdbizz

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import com.maestro.tdbizz.helper.Constant
import com.maestro.tdbizz.signup.SignUpResponse
import io.paperdb.Paper

class SplashActivity : AppCompatActivity() {



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)


        Paper.init(applicationContext)
        val signedUser = Paper.book().read<SignUpResponse>(Constant.USER_DETAILS)




        Handler().postDelayed({

            if (signedUser==null||signedUser.id.isEmpty()){
                startActivity(Intent(applicationContext, LoginActivity::class.java))

            }else {

                startActivity(Intent(applicationContext, MainActivity::class.java))

            }

        },2000)




    }
}