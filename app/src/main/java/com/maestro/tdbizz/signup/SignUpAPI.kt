package com.maestro.tdbizz.signup

import com.maestro.tdbizz.signup.SignUpResponse
import com.maestro.tdbizz.signup.VerifyOtpResponse
import retrofit2.Response
import retrofit2.http.FieldMap
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST
import retrofit2.http.Query

interface SignUpAPI {


    @POST("api/process.php")
    @FormUrlEncoded
    suspend fun register(@Query("action") action:String?, @FieldMap data:HashMap<String,Any>):Response<SignUpResponse>

    @POST("api/process.php")
    @FormUrlEncoded
    suspend fun login(@Query("action") action:String?, @FieldMap data:HashMap<String,Any>):Response<SignUpResponse>

    @POST("api/process.php")
    @FormUrlEncoded
    suspend fun verifyOTP(@Query("action") action:String?, @FieldMap data:HashMap<String,Any>):Response<VerifyOtpResponse>


}