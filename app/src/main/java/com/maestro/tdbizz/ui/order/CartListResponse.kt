package com.maestro.tdbizz.ui.order


import com.google.gson.annotations.SerializedName

data class CartListResponse(
    @SerializedName("color_id")
    val colorId: String,
    @SerializedName("grand_total")
    val grandTotal: String,
    @SerializedName("id")
    val id: String,
    @SerializedName("image")
    val image: String,
    @SerializedName("path")
    val path: String,
    @SerializedName("payment_date")
    val paymentDate: String,
    @SerializedName("payment_status")
    val paymentStatus: String,
    @SerializedName("payment_time")
    val paymentTime: String,
    @SerializedName("payment_type")
    val paymentType: String,
    @SerializedName("Price")
    val price: String,
    @SerializedName("product_id")
    val productId: String,
    @SerializedName("product_name")
    val productName: String,
    @SerializedName("quantity")
    val quantity: String,
    @SerializedName("shipping_address_id")
    val shippingAddressId: String,
    @SerializedName("size_id")
    val sizeId: String,
    @SerializedName("sub_total")
    val subTotal: String,
    @SerializedName("user_id")
    val userId: String,
    @SerializedName("vendor_id")
    val vendorId: String
)