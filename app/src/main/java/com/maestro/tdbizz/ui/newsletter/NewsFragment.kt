package com.maestro.tdbizz.ui.newsletter

import android.app.AlertDialog
import android.content.DialogInterface
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.maestro.tdbizz.R
import com.maestro.tdbizz.databinding.FragmentNewsBinding
import com.maestro.tdbizz.helper.Constant
import com.maestro.tdbizz.ui.newsletter.adapter.NewsAdapter


class NewsFragment : Fragment() {


    lateinit var binding:FragmentNewsBinding
    private val newsViewModel:NewsViewModel by viewModels()
    lateinit var newsAdapter: NewsAdapter

    companion object {

        var lifeCycleMethodCalledInterface:LifeCycleMethodCalledInterface?=null
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentNewsBinding.inflate(layoutInflater)
        // Inflate the layout for this fragment


        newsViewModel.getAllBlogs(Constant.getUserID(requireContext()).id)
        newsViewModel.getSubscribeStatus(Constant.getUserID(requireContext()).id)



        binding.subscribeButton.setOnClickListener {

            val dialog = AlertDialog.Builder(requireActivity()).setTitle("Subscribe").setMessage("Do you want to subscribe the news letter?")
                .setPositiveButton("Yes", object :DialogInterface.OnClickListener{
                    override fun onClick(dialog: DialogInterface?, which: Int) {


                        newsViewModel.addSubscribeOnBlogs(Constant.getUserID(requireContext()).id, Constant.getUserID(requireContext()).email)


                    }

                }).setNegativeButton("No", object :DialogInterface.OnClickListener{
                    override fun onClick(dialog: DialogInterface?, which: Int) {
                        dialog?.dismiss()
                    }

                }).create()

            dialog.show()



        }

        lifecycleScope.launchWhenStarted {


            newsViewModel.newsEvent.observe(viewLifecycleOwner, Observer {

                when(it){

                    is NewsViewModel.NewsEvent.Success -> {

                        activity?.findViewById<ProgressBar>(R.id.loadingBar)?.isVisible = false

                        binding.newsList.apply {

                            newsAdapter =  NewsAdapter(it.blogs,this@NewsFragment,
                                newsViewModel)
                            layoutManager = LinearLayoutManager(requireContext())
                            adapter =newsAdapter
                        }



                    }


                    is NewsViewModel.NewsEvent.SuccessLikeAdded -> {
                        activity?.findViewById<ProgressBar>(R.id.loadingBar)?.isVisible = false

                        if (it.newsLikeCommentAddedResponse.comment != null) {
                            Toast.makeText(
                               requireActivity(),
                                "Comment sent",
                                Toast.LENGTH_SHORT
                            ).show()
                        }

                    }
                    is NewsViewModel.NewsEvent.SuccessBlogSubscribe -> {
                        activity?.findViewById<ProgressBar>(R.id.loadingBar)?.isVisible = false

                        Toast.makeText(requireContext(), ""+it.blogs.result, Toast.LENGTH_SHORT).show()

                    }

                    is NewsViewModel.NewsEvent.SuccessBlogStatusSubscribe -> {
                        activity?.findViewById<ProgressBar>(R.id.loadingBar)?.isVisible = false


                        if (it.data.subsribeStatus==1){


                            binding.subscribeButton.visibility = View.GONE
                        }else {
                            binding.subscribeButton.visibility = View.VISIBLE
                        }

                    }

                    is NewsViewModel.NewsEvent.SuccessUpdated -> {
                        activity?.findViewById<ProgressBar>(R.id.loadingBar)?.isVisible = false

                        newsAdapter.refreshList(it.blogs)
                        

                    }
                    is NewsViewModel.NewsEvent.Loading -> {

                        activity?.findViewById<ProgressBar>(R.id.loadingBar)?.isVisible = true


                    }

                }


            })





        }









        return binding.root
    }


    override fun onPause() {
        super.onPause()

        lifeCycleMethodCalledInterface?.onPause()
    }


}