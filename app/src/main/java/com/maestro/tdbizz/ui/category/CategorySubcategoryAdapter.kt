package com.maestro.tdbizz.ui.category

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.maestro.tdbizz.R
import com.maestro.tdbizz.databinding.CategoryItemBinding
import com.maestro.tdbizz.helper.Constant

class CategorySubcategoryAdapter<T>(val list: List<T>) :
    RecyclerView.Adapter<CategorySubcategoryAdapter.MyViewHolder>() {
    class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        val binding = CategoryItemBinding.bind(view)


        fun setData(categoryResponseItem: Any?) {

            if (categoryResponseItem is CategoryResponseItem) {
                binding.cateTitle.text = categoryResponseItem.categoryName
                Glide.with(binding.root.context)
                    .load(Constant.BASE_URL_IMAGE + categoryResponseItem.image)
                    .placeholder(R.drawable.logo).error(R.drawable.logo).into(binding.catImage)


                binding.root.setOnClickListener {

                    val bundle = Bundle()
                    bundle.putString("id",categoryResponseItem.id)
                    Navigation.findNavController(it).navigate(R.id.subcategoryFragment,bundle)


                }


            }else if (categoryResponseItem is SubCategoryResponseItem){


                binding.cateTitle.text = categoryResponseItem.sub_category_name
                Glide.with(binding.root.context)
                    .load(Constant.BASE_URL_IMAGE + categoryResponseItem.image)
                    .placeholder(R.drawable.logo).error(R.drawable.logo).into(binding.catImage)



                binding.root.setOnClickListener {

                    val bundle = Bundle()
                    bundle.putString("id",categoryResponseItem.id)
                    Navigation.findNavController(it).navigate(R.id.productListFragment,bundle)


                }

            }
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        return MyViewHolder(
            CategoryItemBinding.inflate(LayoutInflater.from(parent.context)).root
        )
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {


        holder.setData(list.get(position))

    }

}