package com.maestro.tdbizz

import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.GravityCompat
import androidx.navigation.NavController
import androidx.navigation.NavDestination
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.google.android.material.navigation.NavigationView
import com.maestro.tdbizz.databinding.ActivityMainBinding
import com.maestro.tdbizz.helper.Constant
import com.maestro.tdbizz.signup.SignUpResponse
import com.vanniktech.emoji.EmojiManager
import com.vanniktech.emoji.google.GoogleEmojiProvider
import io.paperdb.Paper


class MainActivity : AppCompatActivity() {

    private lateinit var appBarConfiguration: AppBarConfiguration
    lateinit var binding:ActivityMainBinding
    lateinit var navController:NavController

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setSupportActionBar(binding.appBarMain.toolbar)

        val navView: NavigationView = findViewById(R.id.nav_view)
         navController = findNavController(R.id.nav_host_fragment)

        appBarConfiguration = AppBarConfiguration(
            setOf(
                R.id.nav_home
            ), binding.drawerLayout
        )



        setupActionBarWithNavController(navController,appBarConfiguration)
        navView.setupWithNavController(navController)

        EmojiManager.install(GoogleEmojiProvider())



        navController.addOnDestinationChangedListener(object:NavController.OnDestinationChangedListener{
            override fun onDestinationChanged(
                controller: NavController,
                destination: NavDestination,
                arguments: Bundle?
            ) {

                when(destination.id){

                    R.id.nav_home -> {
                        binding.appBarMain.toolbar.visibility = View.VISIBLE

                    }


                    R.id.productDetailFragment -> {
                        binding.appBarMain.toolbar.visibility = View.GONE

//                        binding.appBarMain.appBarRoot.visibility = View.GONE
                    }




                    else -> {
                        binding.appBarMain.toolbar.visibility = View.VISIBLE
                        binding.appBarMain.appBarRoot.visibility = View.VISIBLE


                    }

                }

            }

        })


        binding.appBarMain.bottomNavigation.setOnNavigationItemSelectedListener {

            when(it.itemId){

                R.id.profile -> {

                    navController.navigate(R.id.profileFragment)
                     true
                }

                R.id.wishlist -> {

                    navController.navigate(R.id.wishlistFragment)
                    true
                }

                R.id.cart -> {

                    navController.navigate(R.id.cartFragment)
                    true
                }

                R.id.home -> {

                    navController.navigate(R.id.nav_home)
                    true
                }



                else -> {
                    true
                }


            }
        }




        val vendorMenus = binding.navView.menu.findItem(R.id.vendorMenu)

        Paper.init(this)
        val isVendor = Paper.book().read<SignUpResponse>(Constant.USER_DETAILS)

        if (isVendor.isVendor.equals("1")){

            vendorMenus.isVisible = true
        }else {
            vendorMenus.isVisible = false
        }




        binding.navView.setNavigationItemSelectedListener(object:NavigationView.OnNavigationItemSelectedListener{
            override fun onNavigationItemSelected(item: MenuItem): Boolean {

                when(item.itemId){

                    R.id.nav_profile -> {

                        navController.navigate(R.id.profileFragment)
                        binding.drawerLayout.closeDrawer(GravityCompat.START)
                        return true
                    }
                    R.id.nav_categories -> {

                        navController.navigate(R.id.categoryFragment)

                        binding.drawerLayout.closeDrawer(GravityCompat.START)

                        return true
                    }

                    R.id.nav_shops -> {

                        navController.navigate(R.id.vendorsListFragment)
                        binding.drawerLayout.closeDrawer(GravityCompat.START)

                        return true
                    }
                    R.id.nav_logout -> {

                        openAlertLogoutDialog()
                        binding.drawerLayout.closeDrawer(GravityCompat.START)

                        return true
                    }

                    R.id.nav_upload_product -> {
                        navController.navigate(R.id.vendorAddProductScreen)
                        binding.drawerLayout.closeDrawer(GravityCompat.START)


                        binding.appBarMain.bottomNavigation.visibility = View.GONE

                        return true
                    }



                    R.id.nav_my_store -> {
                        val bundle = Bundle()
                        bundle.putString("id",Constant.getUserID(this@MainActivity).id)
                        navController.navigate(R.id.vendorStoreFragment,bundle)
                        binding.drawerLayout.closeDrawer(GravityCompat.START)
                        binding.appBarMain.bottomNavigation.visibility = View.GONE

                        return true
                    }
                    R.id.nav_store_settings -> {
                        navController.navigate(R.id.changeStoreSettingFragment)
                        binding.drawerLayout.closeDrawer(GravityCompat.START)
                        binding.appBarMain.bottomNavigation.visibility = View.GONE

                        return true
                    }

                    else ->{
                        binding.appBarMain.bottomNavigation.visibility = View.VISIBLE

                        binding.drawerLayout.closeDrawer(GravityCompat.START)

                        return false
                    }

                }


            }

        })



    }

    private fun openAlertLogoutDialog() {

        val builder = AlertDialog.Builder(this@MainActivity).setTitle("Log out").setMessage("Are you sure want to log out?")
            .setPositiveButton("YES", object:DialogInterface.OnClickListener{
                override fun onClick(dialog: DialogInterface?, which: Int) {

                    Paper.init(applicationContext)
                    Paper.book().read<SignUpResponse>(Constant.USER_DETAILS)


                    startActivity(Intent(this@MainActivity,LoginActivity::class.java).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP).addFlags(
                        Intent.FLAG_ACTIVITY_NEW_TASK))




                }

            }).setNegativeButton("No", object :DialogInterface.OnClickListener{
                override fun onClick(dialog: DialogInterface?, which: Int) {

                    dialog?.dismiss()
                }

            })

        builder.create().show()


    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.main, menu)
        return true
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        if (item.itemId==R.id.action_search){

          navController.navigate(R.id.searchFragment)

        }



        return super.onOptionsItemSelected(item)



    }


    override fun onBackPressed() {
        super.onBackPressed()

    }

    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.nav_host_fragment)
        return navController.navigateUp(appBarConfiguration) || super.onSupportNavigateUp()
    }
}