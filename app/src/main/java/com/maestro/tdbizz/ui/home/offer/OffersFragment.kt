package com.maestro.tdbizz.ui.home.offer

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.maestro.tdbizz.R
import com.maestro.tdbizz.databinding.FragmentOffersBinding
import com.maestro.tdbizz.ui.home.data.Home
import com.maestro.tdbizz.ui.home.data.HomeViewModel


class OffersFragment : Fragment() {


    lateinit var binding:FragmentOffersBinding
    private val homeViewModel: HomeViewModel by viewModels()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = FragmentOffersBinding.inflate(layoutInflater)

        homeViewModel.getOffers()



        lifecycleScope.launchWhenStarted {

            homeViewModel.homeEvent.observe(viewLifecycleOwner, Observer {

                when(it){


                    is HomeViewModel.HomeEvent.SuccessOffers -> {
                        activity?.findViewById<ProgressBar>(R.id.loadingBar)?.isVisible = false

                        binding.offersList.apply {
                            layoutManager = LinearLayoutManager(context)
                            adapter = OfferAdapter(it.offers)

                        }


                    }

                    is HomeViewModel.HomeEvent.Loading -> {

                        activity?.findViewById<ProgressBar>(R.id.loadingBar)?.isVisible = true

                    }


                    is HomeViewModel.HomeEvent.Failure -> {
                        activity?.findViewById<ProgressBar>(R.id.loadingBar)?.isVisible = false

                        val offerlist = listOf<OfferResponseItem>(
                            OfferResponseItem(id="1",image = "I/81zAJVxHcLL._SL1500_.jpg",path ="https://images-na.ssl-images-amazon.com/images/"
                            ,title = "Massanger"),
                            OfferResponseItem(id="1",image = "I/81zAJVxHcLL._SL1500_.jpg",path ="https://images-na.ssl-images-amazon.com/images/"
                                ,title = "Massanger"),
                            OfferResponseItem(id="1",image = "I/81zAJVxHcLL._SL1500_.jpg",path ="https://images-na.ssl-images-amazon.com/images/"
                                ,title = "Massanger"),
                            OfferResponseItem(id="1",image = "I/81zAJVxHcLL._SL1500_.jpg",path ="https://images-na.ssl-images-amazon.com/images/"
                                ,title = "Massanger"),
                            OfferResponseItem(id="1",image = "I/81zAJVxHcLL._SL1500_.jpg",path ="https://images-na.ssl-images-amazon.com/images/"
                                ,title = "Massanger")
                        )
                        binding.offersList.apply {
                            layoutManager = LinearLayoutManager(context)
                            adapter = OfferAdapter(offerlist)

                        }



                    }


                }









            })


        }



        return binding.root
    }

//    companion object {
//        /**
//         * Use this factory method to create a new instance of
//         * this fragment using the provided parameters.
//         *
//         * @param param1 Parameter 1.
//         * @param param2 Parameter 2.
//         * @return A new instance of fragment OffersFragment.
//         */
//        // TODO: Rename and change types and number of parameters
//        @JvmStatic
//        fun newInstance(param1: String, param2: String) =
//            OffersFragment().apply {
//                arguments = Bundle().apply {
//                    putString(ARG_PARAM1, param1)
//                    putString(ARG_PARAM2, param2)
//                }
//            }
//    }
}