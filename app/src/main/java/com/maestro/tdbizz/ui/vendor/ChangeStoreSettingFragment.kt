package com.maestro.tdbizz.ui.vendor

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import com.bumptech.glide.Glide
import com.github.dhaval2404.imagepicker.ImagePicker
import com.maestro.tdbizz.R
import com.maestro.tdbizz.databinding.FragmentChangeStoreSettingBinding
import com.maestro.tdbizz.helper.Constant
import com.maestro.tdbizz.ui.vendor.data.VendorViewModel
import java.io.File

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [ChangeStoreSettingFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class ChangeStoreSettingFragment : Fragment() {


    lateinit var binding: FragmentChangeStoreSettingBinding

    private val vendorViewModel:VendorViewModel by viewModels()
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentChangeStoreSettingBinding.inflate(layoutInflater)

        vendorViewModel.getVendorDetails(Constant.getUserID(requireContext()).id)


        binding.updateStoreButton.setOnClickListener {

            if (binding.storeNameEdit.text!!.isNotEmpty() &&binding.userNameEdit.text!!.isNotEmpty()){

                vendorViewModel.changeVendorDetails(
                    Constant.getUserID(requireContext()).id,
                    binding.userNameEdit.text.toString(),
                    binding.storeNameEdit.text.toString()
                )




            }


        }


        lifecycleScope.launchWhenStarted {





            vendorViewModel.imageFile.observe(viewLifecycleOwner, Observer {



                binding.updateStoreButton.setOnClickListener { v->


                    vendorViewModel.changeVendorStoreImage(
                        Constant.getUserID(requireContext()).id,it
                    )
                }




            })




            vendorViewModel.vendorEvent.observe(viewLifecycleOwner, Observer {


                when(it){

                    is VendorViewModel.HomeEvent.SuccessVendorDetail -> {


                        binding.storeNameEdit.setText(it.vendors.name)
                        binding.userNameEdit.setText(it.vendors.storeName)



                        Glide.with(requireContext()).load(Constant.BASE_URL_IMAGE+it.vendors.storeImage).error(
                            R.drawable.logo
                        ).into(binding.storeImage)

                    }

                }

            })



        }




        binding.storeImage.setOnClickListener {

            ImagePicker.with(this)
                .galleryOnly()
                .compress(1024)
                .start()


        }




        return binding.root
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            //Image Uri will not be null for RESULT_OK
            val fileUri = data?.data
            binding.storeImage.setImageURI(fileUri)
            //You can get File object from intent
            val file: File = ImagePicker.getFile(data)!!
            //You can also get File Path from intent
            vendorViewModel.sendImageFile(file)


            val filePath: String = ImagePicker.getFilePath(data)!!
        } else if (resultCode == ImagePicker.RESULT_ERROR) {
            Toast.makeText(context, ImagePicker.getError(data), Toast.LENGTH_SHORT).show()
        } else {
            Toast.makeText(context, "Task Cancelled", Toast.LENGTH_SHORT).show()
        }
    }

}