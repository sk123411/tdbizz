package com.maestro.tdbizz.ui.vendor.data

import android.util.Log
import com.maestro.tdbizz.helper.Constant
import com.maestro.tdbizz.helper.Resource
import com.maestro.tdbizz.ui.home.ProductResponse
import com.maestro.tdbizz.ui.home.ProductResponseItem
import com.maestro.tdbizz.ui.product.model.ProductAddedResponse
import com.maestro.tdbizz.ui.vendor.model.VendorDetailResponse
import com.maestro.tdbizz.ui.vendor.model.VendorResponse
import okhttp3.MultipartBody
import okhttp3.RequestBody

class VendorImplementation constructor(var vendorAPI: VendorAPI) : Vendor {
    override suspend fun getAllVendors(): Resource<List<VendorResponse>> {

        return try {

            val response = vendorAPI.getAllVendors(Constant.API_GET_ALL_VENDOR)


            val result = response.body()
            Log.d("XXXXXXXXX", "" + result.toString())

            if (response.isSuccessful && result != null) {
                Log.d("XXXXXXXXX", "" + result.toString())

                Resource.Success(result)


            } else {
                Resource.Error(response.message())
            }
        } catch (e: Exception) {
            Resource.Error(e.message ?: "An error occured")
        }


    }

    override suspend fun addProduct(
        image: MultipartBody.Part?,
        name: RequestBody?,
        desc: RequestBody?,
        price: RequestBody?,
        brand: RequestBody?,
        size: RequestBody?,
        color: RequestBody?,
        category_id: RequestBody?,
        subcategory_id: RequestBody?,
        arrival_status: RequestBody?,
        vendorID: RequestBody?
    ): Resource<ProductResponseItem> {

        return try {

            val response = vendorAPI.addProduct(
                Constant.API_ADD_PRODUCT,
                image,
                name,
                desc,
                price,
                brand,
                size,
                color,
                category_id,
                subcategory_id,
                arrival_status,
                vendorID
            )


            val result = response.body()
            Log.d("XXXXXXXXX", "" + result.toString())

            if (response.isSuccessful && result != null) {
                Log.d("XXXXXXXXX", "" + result.toString())

                Resource.Success(result)


            } else {
                Resource.Error(response.message())
            }
        } catch (e: Exception) {
            Resource.Error(e.message ?: "An error occured")
        }
    }

    override suspend fun getVendorDetails(data: HashMap<String, String>): Resource<VendorDetailResponse> {
        return try {

            val response = vendorAPI.getVendorDetails(Constant.API_GET_VENDOR_DETAIL, data)


            val result = response.body()
            Log.d("XXXXXXXXX", "" + result.toString())

            if (response.isSuccessful && result != null) {
                Log.d("XXXXXXXXX", "" + result.toString())

                Resource.Success(result)


            } else {
                Resource.Error(response.message())
            }
        } catch (e: Exception) {
            Resource.Error(e.message ?: "An error occured")
        }
    }

    override suspend fun getVendorProducts(data: HashMap<String, String>): Resource<ProductResponse> {
        return try {
            val response = vendorAPI.getVendorProducts(Constant.API_GET_VENDOR_PRODUCTS, data)


            val result = response.body()
            Log.d("XXXXXXXXX", "" + result.toString())

            if (response.isSuccessful && result != null) {
                Log.d("XXXXXXXXX", "" + result.toString())

                Resource.Success(result)


            } else {
                Resource.Error(response.message())
            }

        } catch (e: Exception) {
            Resource.Error(e.message ?: "An error occured")
        }
    }

    override suspend fun changeVendorDetails(data: HashMap<String, String>): Resource<VendorDetailResponse> {
        return try {
            val response = vendorAPI.changeVendorDetails(Constant.API_CHANGE_VENDOR_DETAILS, data)


            val result = response.body()
            Log.d("XXXXXXXXX", "" + result.toString())

            if (response.isSuccessful && result != null) {
                Log.d("XXXXXXXXX", "" + result.toString())

                Resource.Success(result)


            } else {
                Resource.Error(response.message())
            }

        } catch (e: Exception) {
            Resource.Error(e.message ?: "An error occured")
        }    }

    override suspend fun changeVendorStoreImage(
        image: MultipartBody.Part?,
        id: RequestBody?
    ): Resource<VendorDetailResponse> {
        return try {
            val response = vendorAPI.changeVendorStoreImage(Constant.API_CHANGE_VENDOR_DETAILS, image,id)


            val result = response.body()
            Log.d("XXXXXXXXX", "" + result.toString())

            if (response.isSuccessful && result != null) {
                Log.d("XXXXXXXXX", "" + result.toString())

                Resource.Success(result)


            } else {
                Resource.Error(response.message())
            }

        } catch (e: Exception) {
            Resource.Error(e.message ?: "An error occured")
        }    }


}