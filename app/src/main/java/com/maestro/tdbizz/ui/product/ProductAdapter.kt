package com.maestro.tdbizz.ui.product

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.maestro.tdbizz.R
import com.maestro.tdbizz.databinding.ProductItemBinding
import com.maestro.tdbizz.helper.Constant
import com.maestro.tdbizz.ui.home.ProductResponseItem
import io.paperdb.Paper
import java.lang.Exception

class ProductAdapter(val list: List<ProductResponseItem>) :RecyclerView.Adapter<ProductAdapter.MyViewHolder>(){
    class MyViewHolder(view:View):RecyclerView.ViewHolder(view) {

        val binding = ProductItemBinding.bind(view)

        fun setData(productResponseItem: ProductResponseItem){


            try {

                Glide.with(binding.root.context)
                    .load(Constant.BASE_URL_IMAGE + productResponseItem.images.get(0).image)
                    .placeholder(
                        R.drawable.logo
                    ).error(R.drawable.logo).into(binding.prImage)
            }catch (e:Exception){
                binding.prImage.setImageResource(R.drawable.logo)
            }

            binding.prTitle.text = productResponseItem.name
            binding.prPrice.text = "Rs " + productResponseItem.price
            binding.root.setOnClickListener {

                Paper.init(it.context)
                Paper.book().write(Constant.PRODUCT_DETAILS,productResponseItem)

                Navigation.findNavController(it).navigate(R.id.productDetailFragment)
            }

            }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
       return MyViewHolder(
           ProductItemBinding.inflate(LayoutInflater.from(parent.context)).root
       )
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {



        holder.setData(list.get(position))



    }

}