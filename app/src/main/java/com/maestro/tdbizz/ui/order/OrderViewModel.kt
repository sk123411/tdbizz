package com.maestro.tdbizz.ui.order

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.maestro.tdbizz.helper.Resource
import com.maestro.tdbizz.retrofit.RetrofitInstance
import com.maestro.tdbizz.ui.home.data.Home
import com.maestro.tdbizz.ui.home.data.HomeAPI
import com.maestro.tdbizz.ui.home.data.HomeImplementation
import com.maestro.tdbizz.ui.home.data.HomeViewModel
import kotlinx.coroutines.launch

class OrderViewModel :ViewModel() {


    private val orderEvent_ = MutableLiveData<OrderEvent>()
    var orderAPI: OrderAPI? = null
    var order:Order? = null


    val orderEvent: LiveData<OrderEvent>
        get() = orderEvent_


    init {
        orderAPI = RetrofitInstance.getRetrofitInstance().create(OrderAPI::class.java)
        order = OrderImplementation(orderAPI!!)


    }


    sealed class OrderEvent {

        class Success(val productToCartResponse: ProductToCartResponse):OrderEvent()
        class SuccessCartOrders(val productToCartResponses: List<CartListResponse>):OrderEvent()

        class Failure(val message: String):OrderEvent()
        object Loading:OrderEvent()
    }



    fun addToCart(productID:String?,userID:String?,quantity:String?){


        val map = HashMap<String,String>()
        map.put("user_id",userID!!)
        map.put("product_id",productID!!)
        map.put("quantity", quantity!!)



        viewModelScope.launch {
            orderEvent_.value = OrderEvent.Loading

            val response = order!!.addToCart(data = map)

            when (response) {

                is Resource.Success -> {


                    orderEvent_.value = OrderViewModel.OrderEvent.Success(response.data!!)


                }

                is Resource.Error -> {
                    orderEvent_.value = OrderViewModel.OrderEvent.Failure("Server error occured"!!)



                }


            }


        }






    }


    fun getCart(userID:String?){


        val map = HashMap<String,String>()
        map.put("user_id",userID!!)




        viewModelScope.launch {
            orderEvent_.value = OrderEvent.Loading

            val response = order!!.getCart(data = map)

            when (response) {

                is Resource.Success -> {


                    orderEvent_.value = OrderViewModel.OrderEvent.SuccessCartOrders(response.data!!)


                }

                is Resource.Error -> {
                    orderEvent_.value = OrderViewModel.OrderEvent.Failure("Server error occured"!!)



                }


            }


        }






    }

    fun updateCart(productID:String?,userID:String?,quantity:String?){


        val map = HashMap<String,String>()
        map.put("user_id",userID!!)
        map.put("product_id",productID!!)
        map.put("quantity", quantity!!)



        viewModelScope.launch {
            orderEvent_.value = OrderEvent.Loading

            val response = order!!.updateCart(data = map)

            when (response) {

                is Resource.Success -> {


                    orderEvent_.value = OrderViewModel.OrderEvent.Success(response.data!!)

                    getCart(userID)


                }

                is Resource.Error -> {
                    orderEvent_.value = OrderViewModel.OrderEvent.Failure("Server error occured"!!)



                }


            }


        }






    }

    fun deleteProductFromCart(productID:String?,userID:String?){


        val map = HashMap<String,String>()
        map.put("user_id",userID!!)
        map.put("product_id",productID!!)



        viewModelScope.launch {
            orderEvent_.value = OrderEvent.Loading

            val response = order!!.deleteProductFromCart(data = map)

            when (response) {

                is Resource.Success -> {


                    orderEvent_.value = OrderViewModel.OrderEvent.Success(response.data!!)

                    getCart(userID)
                }

                is Resource.Error -> {
                    orderEvent_.value = OrderViewModel.OrderEvent.Failure("Server error occured"!!)



                }


            }


        }






    }
}