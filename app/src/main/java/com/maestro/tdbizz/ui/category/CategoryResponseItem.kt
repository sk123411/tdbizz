package com.maestro.tdbizz.ui.category


import com.google.gson.annotations.SerializedName

data class CategoryResponseItem(
    @SerializedName("category_name")
    val categoryName: String,
    @SerializedName("id")
    val id: String,
    @SerializedName("image")
    val image: String,
    @SerializedName("path")
    val path: String
)