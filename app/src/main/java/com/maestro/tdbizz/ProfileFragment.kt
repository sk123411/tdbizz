package com.maestro.tdbizz

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.RadioGroup
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import com.maestro.tdbizz.databinding.FragmentProfileBinding
import com.maestro.tdbizz.helper.Constant
import com.maestro.tdbizz.ui.home.data.HomeViewModel


class ProfileFragment : Fragment() {

    lateinit var binding: FragmentProfileBinding
    var gender:String?=null
    private val homeViewModel: HomeViewModel by viewModels()
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = FragmentProfileBinding.inflate(layoutInflater)

        homeViewModel.getProfile(Constant.getUserID(requireContext()).id)



        binding.genderGroup.setOnCheckedChangeListener(object :RadioGroup.OnCheckedChangeListener{
            override fun onCheckedChanged(group: RadioGroup?, checkedId: Int) {


                when(checkedId){

                    R.id.maleRadio -> {
                        gender = "Male"

                    }

                    R.id.femaleRadio -> {

                        gender = "Female"
                    }
                }

            }

        })



        lifecycleScope.launchWhenStarted {


            homeViewModel.homeEvent.observe(viewLifecycleOwner, Observer {


                when (it) {

                    is HomeViewModel.HomeEvent.SuccessProfile -> {
                        activity?.findViewById<ProgressBar>(R.id.loadingBar)?.isVisible = false

                        if (it.profile != null) {

                            binding.emailEdit.setText(it.profile.email)
                            binding.mobileEdit.setText(it.profile.mobile)
                            binding.firstNameEdit.setText(it.profile.firstName)
                            binding.lastNameEdit.setText(it.profile.lastName)


                            if (it.profile.gender.equals("Male")){
                                binding.maleRadio.isChecked = true
                            }else {
                                binding.femaleRadio.isChecked = true
                            }
                        }


                    }


                    is HomeViewModel.HomeEvent.Loading -> {

                        activity?.findViewById<ProgressBar>(R.id.loadingBar)?.isVisible = true

                    }


                }


            })


        }




        binding.saveButton.setOnClickListener {


            if (binding.emailEdit.text.toString().isNotEmpty() &&
                binding.firstNameEdit.text.toString().isNotEmpty() &&
                binding.lastNameEdit.text.toString().isNotEmpty() &&
                binding.mobileEdit.text.toString().isNotEmpty()
                    ) {


                homeViewModel.updateProfile(
                    "80",
                    binding.firstNameEdit.text.toString(),
                    binding.lastNameEdit.text.toString(),
                    binding.mobileEdit.text.toString(),
                    binding.emailEdit.text.toString(),
                    gender
                )


            }


        }

        // Inflate the layout for this fragment


        return binding.root
    }


}