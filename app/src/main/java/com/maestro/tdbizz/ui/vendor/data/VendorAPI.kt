package com.maestro.tdbizz.ui.vendor.data

import com.maestro.tdbizz.ui.category.CategoryResponse
import com.maestro.tdbizz.ui.category.SubCategoryResponseItem
import com.maestro.tdbizz.ui.home.ProductResponse
import com.maestro.tdbizz.ui.home.ProductResponseItem
import com.maestro.tdbizz.ui.vendor.model.VendorDetailResponse
import com.maestro.tdbizz.ui.vendor.model.VendorResponse
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Response
import retrofit2.http.*

interface VendorAPI {


    @GET("api/process.php")
    suspend fun getAllVendors(@Query("action") action: String?): Response<List<VendorResponse>>


    @Multipart
    @POST("api/process.php")
    suspend fun addProduct(@Query("action") updateProfile:String?,
                           @Part image: MultipartBody.Part?,
                           @Part("name") name: RequestBody?,
                           @Part("desc") desc: RequestBody?,
                           @Part("price") price: RequestBody?,
                           @Part("brand") brand: RequestBody?,
                           @Part("size") size: RequestBody?,
                           @Part("color") color: RequestBody?,
                           @Part("category_id") category_id: RequestBody?,
                           @Part("subcategory_id") subcategory_id: RequestBody?,
                           @Part("arrival_status") arrival_status: RequestBody?,
                           @Part("vendor_id") vendorID: RequestBody?

                           ):

            Response<ProductResponseItem>





    @POST("api/process.php")
    @FormUrlEncoded
    suspend fun getVendorDetails(@Query("action") action: String?, @FieldMap data:HashMap<String,String>): Response<VendorDetailResponse>


    @POST("api/process.php")
    @FormUrlEncoded
    suspend fun getVendorProducts(@Query("action") action: String?, @FieldMap data:HashMap<String,String>): Response<ProductResponse>


    @POST("api/process.php")
    @FormUrlEncoded
    suspend fun changeVendorDetails(@Query("action") action: String?, @FieldMap data:HashMap<String,String>): Response<VendorDetailResponse>

    @POST("api/process.php")
    @Multipart
    suspend fun changeVendorStoreImage(@Query("action") action: String?, @Part image:MultipartBody.Part?, @Part("id") vendorID: RequestBody?): Response<VendorDetailResponse>






}