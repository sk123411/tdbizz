package com.maestro.tdbizz.ui.wishlist

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import androidx.navigation.Navigation
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.maestro.tdbizz.R
import com.maestro.tdbizz.databinding.FragmentWishlistBinding
import com.maestro.tdbizz.helper.Constant
import com.maestro.tdbizz.ui.product.ProductAdapter
import com.maestro.tdbizz.ui.product.data.ProductViewModel

class WishlistFragment : Fragment() {

    lateinit var binding: FragmentWishlistBinding
    private val productViewModel: ProductViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = FragmentWishlistBinding.inflate(layoutInflater)
        productViewModel.showWishlist(Constant.getUserID(requireContext()).id)

        lifecycleScope.launchWhenStarted {

            productViewModel.productEvent.observe(viewLifecycleOwner, Observer {


                when (it) {


                    is ProductViewModel.ProductEvent.Loading -> {


                        activity?.findViewById<ProgressBar>(R.id.loadingBar)?.isVisible = true

                    }


                    is ProductViewModel.ProductEvent.SuccessProducts -> {

                        activity?.findViewById<ProgressBar>(R.id.loadingBar)?.isVisible = false


                        if (it.products.isEmpty()){

                            binding.noWishText.visibility = View.VISIBLE

                        }else {
                            binding.noWishText.visibility = View.GONE

                            binding.wishlist.apply {


                                layoutManager = GridLayoutManager(
                                    context,
                                    2
                                )
                                adapter =
                                    ProductAdapter(it.products)
                            }
                        }


                    }

                }


            })


        }



        if(binding.noWishText.isVisible){

            binding.addProductButton.setOnClickListener {

                Navigation.findNavController(requireView()).navigate(R.id.nav_home)
            }
        }


        return binding.root
    }
}