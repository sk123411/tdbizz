package com.maestro.tdbizz.ui.home


import androidx.annotation.Nullable
import com.google.gson.annotations.SerializedName
import com.maestro.tdbizz.ui.product.model.Slider

data class ProductResponseItem(
    @SerializedName("arrival_status")
    val arrivalStatus: String,
    @SerializedName("brand")
    val brand: String,
    @SerializedName("category_id")
    val categoryId: String,
    @SerializedName("color")
    val color: String,
    @SerializedName("desc")
    val desc: String,
    @SerializedName("id")
    val id: String,
    @SerializedName("images")
    val images: List<Slider>,
    @SerializedName("name")
    val name: String,
    @SerializedName("path")
    val path: String,
    @SerializedName("price")
    val price: String,
    @SerializedName("size")
    val size: String,
    @SerializedName("product_id")
    @Nullable
    val productID: String?,
    @SerializedName("vendor_id")
    @Nullable
    val vendorID: String?




) {
    override fun toString(): String {
        return "ProductResponseItem(arrivalStatus='$arrivalStatus', brand='$brand', categoryId='$categoryId', color='$color', desc='$desc', id='$id', images=$images, name='$name', path='$path', price='$price', size='$size', productID=$productID, vendorID=$vendorID)"
    }
}