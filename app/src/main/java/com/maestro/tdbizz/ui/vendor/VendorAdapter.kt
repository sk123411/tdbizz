package com.maestro.tdbizz.ui.vendor

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.maestro.tdbizz.R
import com.maestro.tdbizz.databinding.ProductItemBinding
import com.maestro.tdbizz.databinding.VendorsItemBinding
import com.maestro.tdbizz.helper.Constant
import com.maestro.tdbizz.ui.home.ProductResponseItem
import com.maestro.tdbizz.ui.vendor.model.VendorResponse
import io.paperdb.Paper

class VendorAdapter(val list: List<VendorResponse>) :
    RecyclerView.Adapter<VendorAdapter.MyViewHolder>() {
    class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        val binding = VendorsItemBinding.bind(view)

        fun setData(productResponseItem: VendorResponse) {


            if (productResponseItem.name.length > 2) {
                binding.catNickTitle.text = productResponseItem.name.toString().substring(0, 2)
            } else {
                binding.catNickTitle.text = productResponseItem.email.substring(0,2)

            }


            val split= productResponseItem.email.split("@")


            binding.cateTitle.text = split.get(0)


            binding.root.setOnClickListener {
                //Paper.init(it.context)
                //Paper.book().write(Constant.PRODUCT_DETAILS,productResponseItem)
                val vendorID = productResponseItem.id
                val bundle = Bundle()
                bundle.putString("id",vendorID)
                Navigation.findNavController(it).navigate(R.id.vendorStoreFragment,bundle)


            }

        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        return MyViewHolder(
            VendorsItemBinding.inflate(LayoutInflater.from(parent.context)).root
        )
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {


        holder.setData(list.get(position))


    }

}