package com.maestro.tdbizz.ui.newsletter


import com.google.gson.annotations.SerializedName

data class SubscribeResponse(
    @SerializedName("email")
    val email: String,
    @SerializedName("id")
    val id: String,
    @SerializedName("result")
    val result: String,
    @SerializedName("user_id")
    val userId: String
)