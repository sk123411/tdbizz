package com.maestro.tdbizz.ui.vendor.model


import androidx.annotation.Nullable
import com.google.gson.annotations.SerializedName

data class VendorDetailResponse(
    @SerializedName("address")
    val address: String,
    @SerializedName("email")
    val email: String,
    @SerializedName("id")
    val id: String,
    @SerializedName("mobile")
    val mobile: String,
    @SerializedName("name")
    val name: String,
    @SerializedName("otp")
    val otp: String,
    @SerializedName("password")
    val password: String,
    @SerializedName("status")
    val status: String,
    @SerializedName("store_name")
    val storeName: String,
    @SerializedName("store_image")
    @Nullable
    val storeImage: String

)