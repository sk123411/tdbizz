package com.maestro.tdbizz.ui.order

import com.maestro.tdbizz.helper.Resource

interface Order {

    suspend fun deleteProductFromCart(data:HashMap<String,String>):Resource<ProductToCartResponse>

    suspend fun addToCart(data:HashMap<String,String>):Resource<ProductToCartResponse>
    suspend fun getCart(data:HashMap<String,String>):Resource<List<CartListResponse>>
    suspend fun updateCart(data:HashMap<String,String>):Resource<ProductToCartResponse>
}