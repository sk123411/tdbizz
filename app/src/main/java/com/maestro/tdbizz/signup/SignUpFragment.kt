package com.maestro.tdbizz.signup

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import androidx.navigation.Navigation
import com.maestro.tdbizz.R
import com.maestro.tdbizz.ui.vendor.VendorsActivity
import com.maestro.tdbizz.databinding.FragmentSignupBinding



class SignUpFragment : Fragment() {

    lateinit var binding: FragmentSignupBinding

    private val signUpViewModel: SignUpViewModel by viewModels()


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentSignupBinding.inflate(layoutInflater)



        binding.signUpButton.setOnClickListener {






            if (binding.emailEdit.text.toString().isEmpty() ||
                binding.mobileEdit.text.toString().isEmpty()
            ) {

                Toast.makeText(context,"Please fill all the required fields",Toast.LENGTH_LONG).show()

            } else {


                if (requireActivity() is VendorsActivity) {
                    signUpViewModel.register(binding.emailEdit.text.toString(), binding.mobileEdit.text.toString(),true)


                } else {

                    signUpViewModel.register(binding.emailEdit.text.toString(), binding.mobileEdit.text.toString(),false)

                }

            }

        }








        lifecycleScope.launchWhenStarted {



            signUpViewModel.signUpEvent.observe(viewLifecycleOwner, Observer {res ->


                when(res){


                   is  SignUpViewModel.SignUpEvent.Success -> {

                       activity?.findViewById<ProgressBar>(R.id.loadingBar)?.isVisible = false

                       if (res.signUpResponse.email==null) {
                           Toast.makeText(
                               context,
                               "Sign up failed! ${res.signUpResponse.result}",
                               Toast.LENGTH_SHORT
                           ).show()


                       }else {

                           Toast.makeText(
                               context,
                               "Sign up successfull",
                               Toast.LENGTH_SHORT
                           ).show()
                       }

                       Navigation.findNavController(requireView()).navigate(R.id.loginFragment2)

                   }

                    is SignUpViewModel.SignUpEvent.Failure -> {


                        activity?.findViewById<ProgressBar>(R.id.loadingBar)?.isVisible = false
                        Toast.makeText(context, "Sign up failed ${res.message}", Toast.LENGTH_SHORT).show()
                    }


                    is SignUpViewModel.SignUpEvent.Loading -> {


                        activity?.findViewById<ProgressBar>(R.id.loadingBar)?.isVisible = true
                    }
                }




            })


        }
















        return binding.root
    }


}