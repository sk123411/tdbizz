package com.maestro.tdbizz.ui.cart

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.maestro.tdbizz.R
import com.maestro.tdbizz.databinding.FragmentCartBinding
import com.maestro.tdbizz.helper.Constant
import com.maestro.tdbizz.ui.order.OrderViewModel

class CartFragment : Fragment() {



    lateinit var binding:FragmentCartBinding

    private val orderViewModel:OrderViewModel by viewModels()
    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {


        binding = FragmentCartBinding.inflate(layoutInflater)

        orderViewModel.getCart(Constant.getUserID(requireContext()).id)
        

        lifecycleScope.launchWhenStarted {




            orderViewModel.orderEvent.observe(viewLifecycleOwner, Observer {


                when(it){



                    is OrderViewModel.OrderEvent.SuccessCartOrders -> {
                        activity?.findViewById<ProgressBar>(R.id.loadingBar)?.isVisible = false



                        if(it.productToCartResponses.size > 0 ) {
                            binding.cartList.apply {
                                layoutManager = LinearLayoutManager(context)
                                adapter = CartAdapter(
                                    it.productToCartResponses.toMutableList(),
                                    orderViewModel
                                )
                            }


                        }
                    }


                    is OrderViewModel.OrderEvent.Success-> {

                        activity?.findViewById<ProgressBar>(R.id.loadingBar)?.isVisible = false

                        Toast.makeText(requireContext(), "Cart updated", Toast.LENGTH_SHORT).show()
                    }



                    is OrderViewModel.OrderEvent.Loading -> {

                        activity?.findViewById<ProgressBar>(R.id.loadingBar)?.isVisible = true


                    }
                }






            })



        }







        return binding.root
    }
}