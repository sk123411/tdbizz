package com.maestro.tdbizz.ui.category

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.maestro.tdbizz.R
import com.maestro.tdbizz.databinding.FragmentCategoryBinding
import com.maestro.tdbizz.ui.home.data.HomeViewModel


class CategoryFragment : Fragment() {


    lateinit var binding: FragmentCategoryBinding
    private val homeViewModel: HomeViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentCategoryBinding.inflate(layoutInflater)
        // Inflate the layout for this fragment

        homeViewModel.getCategories()




        lifecycleScope.launchWhenStarted {


            homeViewModel.homeEvent.observe(viewLifecycleOwner, Observer {


                when (it) {


                    is HomeViewModel.HomeEvent.SuccessCategories -> {
                        activity?.findViewById<ProgressBar>(R.id.loadingBar)?.isVisible = false

                        binding.categoriesList.apply {


                            layoutManager = GridLayoutManager(context, 2)
                            adapter =
                                CategorySubcategoryAdapter(it.categories)

                        }


                    }

                    is HomeViewModel.HomeEvent.Loading -> {


                        activity?.findViewById<ProgressBar>(R.id.loadingBar)?.isVisible = true

                    }


                }


            })


        }





        return binding.root
    }


}