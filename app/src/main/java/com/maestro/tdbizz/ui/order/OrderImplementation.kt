package com.maestro.tdbizz.ui.order

import android.util.Log
import com.maestro.tdbizz.helper.Constant
import com.maestro.tdbizz.helper.Resource

class OrderImplementation(val otderAPI: OrderAPI) : Order {
    override suspend fun deleteProductFromCart(data: HashMap<String, String>): Resource<ProductToCartResponse> {
        return try {

            val response = otderAPI.deleteProductFromCart(Constant.API_DELETE_FROM_CART,data)


            val result = response.body()
            Log.d("XXXXXXXXX", "" + result.toString())

            if (response.isSuccessful && result != null) {
                Log.d("XXXXXXXXX", "" + result.toString())

                com.maestro.tdbizz.helper.Resource.Success(result!!)


            } else {

                Log.d("XXXXXXXXX", "" + response.message())

                com.maestro.tdbizz.helper.Resource.Error(response.message())
            }
        } catch (e: Exception) {

            Log.d("ppppppppppppp", "" + e.message)

            com.maestro.tdbizz.helper.Resource.Error(e.message ?: "An error occured")
        }    }

    override suspend fun addToCart(data: HashMap<String, String>): Resource<ProductToCartResponse> {
        return try {

            val response = otderAPI.addToCart(Constant.API_ADD_TO_CART,data)


            val result = response.body()
            Log.d("XXXXXXXXX", "" + result.toString())

            if (response.isSuccessful && result != null) {
                Log.d("XXXXXXXXX", "" + result.toString())

               com.maestro.tdbizz.helper.Resource.Success(result!!)


            } else {

                Log.d("XXXXXXXXX", "" + response.message())

                com.maestro.tdbizz.helper.Resource.Error(response.message())
            }
        } catch (e: Exception) {

            Log.d("ppppppppppppp", "" + e.message)

            com.maestro.tdbizz.helper.Resource.Error(e.message ?: "An error occured")
        }


    }

    override suspend fun getCart(data: HashMap<String, String>): Resource<List<CartListResponse>> {
        return try {

            val response = otderAPI.getCart(Constant.API_GET_CARTS,data)


            val result = response.body()
            Log.d("XXXXXXXXX", "" + result.toString())

            if (response.isSuccessful && result != null) {
                Log.d("XXXXXXXXX", "" + result.toString())

                com.maestro.tdbizz.helper.Resource.Success(result!!)


            } else {
                com.maestro.tdbizz.helper.Resource.Error(response.message())
            }
        } catch (e: Exception) {
            com.maestro.tdbizz.helper.Resource.Error(e.message ?: "An error occured")
        }
    }

    override suspend fun updateCart(data: HashMap<String, String>): Resource<ProductToCartResponse> {
        return try {

            val response = otderAPI.updateCart(Constant.API_UPDATE_CART,data)


            val result = response.body()
            Log.d("XXXXXXXXX", "" + result.toString())

            if (response.isSuccessful && result != null) {
                Log.d("XXXXXXXXX", "" + result.toString())

                com.maestro.tdbizz.helper.Resource.Success(result!!)


            } else {
                com.maestro.tdbizz.helper.Resource.Error(response.message())
            }
        } catch (e: Exception) {
            com.maestro.tdbizz.helper.Resource.Error(e.message ?: "An error occured")
        }
    }


}