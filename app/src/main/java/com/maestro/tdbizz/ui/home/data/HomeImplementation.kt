package com.maestro.tdbizz.ui.home.data

import android.util.Log
import com.maestro.tdbizz.helper.Constant
import com.maestro.tdbizz.helper.Resource
import com.maestro.tdbizz.ui.category.CategoryResponse
import com.maestro.tdbizz.ui.category.SubCategoryResponseItem
import com.maestro.tdbizz.ui.home.ProductResponse
import com.maestro.tdbizz.ui.home.ProductResponseItem
import com.maestro.tdbizz.ui.home.offer.OfferResponseItem

class HomeImplementation constructor(var homeAPI: HomeAPI) : Home {


    override suspend fun getCategory(): Resource<CategoryResponse> {
        return try {

            val response = homeAPI.getCategory(Constant.API_CATEGORY)


            val result = response.body()
            Log.d("XXXXXXXXX", "" + result.toString())

            if (response.isSuccessful && result != null) {
                Log.d("XXXXXXXXX", "" + result.toString())

                Resource.Success(result)


            } else {
                Resource.Error(response.message())
            }
        } catch (e: Exception) {
            Resource.Error(e.message ?: "An error occured")
        }

    }

    override suspend fun getSubcategory(data: HashMap<String, String>): Resource<List<SubCategoryResponseItem>> {
        return try {

            val response = homeAPI.getSubcategory(Constant.API_SUB_CATEGORY,data)


            val result = response.body()
            Log.d("XXXXXXXXX", "" + result.toString())

            if (response.isSuccessful && result != null) {
                Log.d("XXXXXXXXX", "" + result.toString())

                Resource.Success(result)


            } else {
                Resource.Error(response.message())
            }
        } catch (e: Exception) {
            Resource.Error(e.message ?: "An error occured")
        }    }

    override suspend fun getProducts(): Resource<ProductResponse> {
        return try {

            val response = homeAPI.getProducts(Constant.API_PRODUCT)


            val result = response.body()
            Log.d("XXXXXXXXX", "" + result.toString())

            if (response.isSuccessful && result != null) {
                Log.d("XXXXXXXXX", "" + result.toString())

                Resource.Success(result)


            } else {
                Resource.Error(response.message())
            }
        } catch (e: Exception) {
            Resource.Error(e.message ?: "An error occured")
        }


    }

    override suspend fun getProfile(data: HashMap<String, String>): Resource<ProfileResponse> {
        return try {

            val response = homeAPI.getProfile(Constant.API_PROFILE, data)


            val result = response.body()
            Log.d("XXXXXXXXX", "" + result.toString())

            if (response.isSuccessful && result != null) {
                Log.d("XXXXXXXXX", "" + result.toString())

                Resource.Success(result)


            } else {
                Resource.Error(response.message())
            }
        } catch (e: Exception) {
            Resource.Error(e.message ?: "An error occured")
        }
    }

    override suspend fun updateProfile(data: HashMap<String, String>): Resource<ProfileResponse> {
        return try {

            val response = homeAPI.updateProfile(Constant.API_UPDATE_PROFILE, data)


            val result = response.body()
            Log.d("XXXXXXXXX", "" + result.toString())

            if (response.isSuccessful && result != null) {
                Log.d("XXXXXXXXX", "" + result.toString())

                Resource.Success(result)


            } else {
                Resource.Error(response.message())
            }
        } catch (e: Exception) {
            Resource.Error(e.message ?: "An error occured")
        }
    }

    override suspend fun getFeatureProducts(): Resource<List<ProductResponseItem>> {
        return try {

            val response = homeAPI.getProducts(Constant.API_FEATURE_PRODUCTS)


            val result = response.body()
            Log.d("XXXXXXXXX", "" + result.toString())

            if (response.isSuccessful && result != null) {
                Log.d("XXXXXXXXX", "" + result.toString())

                Resource.Success(result)


            } else {
                Resource.Error(response.message())
            }
        } catch (e: Exception) {
            Resource.Error(e.message ?: "An error occured")
        }

    }

    override suspend fun getOffers(): Resource<List<OfferResponseItem>> {
        return try {

            val response = homeAPI.getOffers(Constant.API_GET_OFFERS)


            val result = response.body()
            Log.d("XXXXXXXXX", "" + result.toString())

            if (response.isSuccessful && result != null) {
                Log.d("XXXXXXXXX", "" + result.toString())

                Resource.Success(result)


            } else {
                Resource.Error(response.message())
            }
        } catch (e: Exception) {
            Resource.Error(e.message ?: "An error occured")
        }
    }


}