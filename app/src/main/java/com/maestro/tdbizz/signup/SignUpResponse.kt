package com.maestro.tdbizz.signup


import androidx.annotation.Nullable
import com.google.gson.annotations.SerializedName

data class SignUpResponse(
    @SerializedName("email")
    @Nullable
    val email: String?,
    @SerializedName("first_name")
    val firstName: String,
    @SerializedName("gender")
    val gender: String,
    @SerializedName("id")
    val id: String,
    @SerializedName("last_name")
    val lastName: String,
    @SerializedName("mobile")
    val mobile: String,
    @SerializedName("otp")
    val otp: String,
    @SerializedName("password")
    val password: String,
    @SerializedName("register_agree")
    val registerAgree: String,
    @SerializedName("result")
    val result: String,
    @SerializedName("isVendor")
    @Nullable
    val isVendor: String?
)