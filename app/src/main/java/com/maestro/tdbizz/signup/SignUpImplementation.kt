package com.maestro.tdbizz.signup

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.Navigation
import com.maestro.tdbizz.helper.Constant
import com.maestro.tdbizz.helper.Resource
import com.maestro.tdbizz.signup.SignUpResponse
import com.maestro.tdbizz.signup.VerifyOtpResponse

import retrofit2.Retrofit

class SignUpImplementation constructor(var signUpAPI: SignUpAPI) : Signup{



    override suspend fun register(data: HashMap<String, Any>): Resource<SignUpResponse> {

        return try {

            val response = signUpAPI.register(Constant.API_SIGNUP,data)


            val result = response.body()
            Log.d("XXXXXXXXX", "" + result.toString())

            if (response.isSuccessful && result != null) {
                Log.d("XXXXXXXXX", "" + result.toString())

                Resource.Success(result)


            } else {
                Resource.Error(response.message())
            }
        } catch (e: Exception) {
            Resource.Error(e.message ?: "An error occured")
        }

    }

    override suspend fun login(data: HashMap<String, Any>): Resource<SignUpResponse> {
        return try {

            val response = signUpAPI.login(Constant.API_LOGIN,data)


            val result = response.body()
            Log.d("XXXXXXXXX", "" + result.toString())

            if (response.isSuccessful && result != null) {
                Log.d("XXXXXXXXX", "" + result.toString())

                Resource.Success(result)


            } else {
                Resource.Error(response.message())
            }
        } catch (e: Exception) {
            Resource.Error(e.message ?: "An error occured")
        }

    }

    override suspend fun verifyOTP(data: HashMap<String, Any>): Resource<VerifyOtpResponse> {
        return try {

            val response = signUpAPI.verifyOTP(Constant.API_OTP_VERIFY,data)


            val result = response.body()
            Log.d("XXXXXXXXX", "" + result.toString())

            if (response.isSuccessful && result != null) {
                Log.d("XXXXXXXXX", "" + result.toString())

                Resource.Success(result)


            } else {
                Resource.Error(response.message())
            }
        } catch (e: Exception) {
            Resource.Error(e.message ?: "An error occured")
        }
    }


}