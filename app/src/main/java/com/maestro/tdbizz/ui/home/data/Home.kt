package com.maestro.tdbizz.ui.home.data

import com.maestro.tdbizz.helper.Resource
import com.maestro.tdbizz.ui.category.CategoryResponse
import com.maestro.tdbizz.ui.category.SubCategoryResponseItem
import com.maestro.tdbizz.ui.home.ProductResponse
import com.maestro.tdbizz.ui.home.ProductResponseItem
import com.maestro.tdbizz.ui.home.offer.OfferResponseItem

interface Home {

    suspend fun getCategory(): Resource<CategoryResponse>
    suspend fun getSubcategory(data: HashMap<String, String>): Resource<List<SubCategoryResponseItem>>
    suspend fun getProducts(): Resource<ProductResponse>
    suspend fun getProfile(data: HashMap<String,String>): Resource<ProfileResponse>
    suspend fun updateProfile(data: HashMap<String, String>): Resource<ProfileResponse>
    suspend fun getFeatureProducts(): Resource<List<ProductResponseItem>>
    suspend fun getOffers(): Resource<List<OfferResponseItem>>

}