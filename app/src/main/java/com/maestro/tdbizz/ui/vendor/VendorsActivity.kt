package com.maestro.tdbizz.ui.vendor

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.navigation.NavController
import androidx.navigation.NavDestination
import androidx.navigation.findNavController
import com.maestro.tdbizz.LoginActivity
import com.maestro.tdbizz.R
import com.maestro.tdbizz.databinding.ActivityVendorsBinding

class VendorsActivity : AppCompatActivity() {

    lateinit var binding: ActivityVendorsBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityVendorsBinding.inflate(layoutInflater)
        setContentView(binding.root)


        val navController = findNavController(R.id.nav_vendors)


        binding.userLoginButton.setOnClickListener {

            startActivity(Intent(applicationContext, LoginActivity::class.java))


        }



        binding.signInTab.setOnClickListener {

            navController.navigate(R.id.loginFragment2)

        }

        binding.signUpTab.setOnClickListener {

            navController.navigate(R.id.signUpFragment2)

        }
        navController.addOnDestinationChangedListener(object :
            NavController.OnDestinationChangedListener {
            override fun onDestinationChanged(
                controller: NavController,
                destination: NavDestination,
                arguments: Bundle?
            ) {

                when (destination.id) {

                    R.id.loginFragment2 -> {

                        binding.signInTab.setBackgroundColor(resources.getColor(R.color.purple_700))
                        binding.signUpTab.setBackgroundColor(resources.getColor(android.R.color.white))
                        binding.signUpText.setTextColor(resources.getColor(R.color.purple_700))
                        binding.signInText.setTextColor(resources.getColor(R.color.white))

                    }
                    R.id.signUpFragment2 -> {

                        binding.signUpTab.setBackgroundColor(resources.getColor(R.color.purple_700))
                        binding.signInTab.setBackgroundColor(resources.getColor(android.R.color.white))
                        binding.signInText.setTextColor(resources.getColor(R.color.purple_700))
                        binding.signUpText.setTextColor(resources.getColor(R.color.white))

                    }



                }


            }

        })
    }
}