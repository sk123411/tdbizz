package com.maestro.tdbizz.ui.home.offer

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.maestro.tdbizz.R
import com.maestro.tdbizz.databinding.LytOfferItemBinding
import com.squareup.picasso.Picasso

class OfferAdapter constructor(val offerLIst:List<OfferResponseItem>) :
    RecyclerView.Adapter<OfferAdapter.MyViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): OfferAdapter.MyViewHolder {
        return MyViewHolder(LytOfferItemBinding.inflate(LayoutInflater.from(parent.context)).root)
    }

    class MyViewHolder(view:View):RecyclerView.ViewHolder(view) {

        val binding = LytOfferItemBinding.bind(view)

        fun bindData(offerResponseItem: OfferResponseItem) {

//            Picasso.get().load(offerResponseItem.path+offerResponseItem.image)
//                .placeholder(R.drawable.logo).into(binding.offerImage)

            Picasso.get().load("https://images-na.ssl-images-amazon.com/images/I/81zAJVxHcLL._SL1500_.jpg")
                .placeholder(R.drawable.logo).into(binding.offerImage)
            binding.offerTitle.text = offerResponseItem.title


        }

    }

    override fun onBindViewHolder(holder: OfferAdapter.MyViewHolder, position: Int) {
        holder.bindData(offerLIst.get(position))
    }

    override fun getItemCount(): Int {

        return offerLIst.size
    }


}
