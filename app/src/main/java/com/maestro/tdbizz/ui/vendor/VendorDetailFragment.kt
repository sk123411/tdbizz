package com.maestro.tdbizz.ui.vendor

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.maestro.tdbizz.databinding.FragmentVendorDetailBinding

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [VendorDetailFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class VendorDetailFragment : Fragment() {
    private lateinit var binding: FragmentVendorDetailBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {


        binding = FragmentVendorDetailBinding.inflate(layoutInflater)



        binding.categoriesList.apply {


            layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL,false)
          //  adapter = CategoryAdapter(list = mutableListOf<Int>(1,2,4,5,6,7,8))
        }




        binding.productsList.apply {


            layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL,false)
           // adapter = ProductAdapter(list = mutableListOf<Int>(1,2,4,5,6,7,8))
        }







        return binding.root
    }
}