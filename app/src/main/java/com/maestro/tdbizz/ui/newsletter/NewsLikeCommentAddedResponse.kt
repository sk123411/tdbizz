package com.maestro.tdbizz.ui.newsletter


import androidx.annotation.Nullable
import com.google.gson.annotations.SerializedName

data class NewsLikeCommentAddedResponse(
    @SerializedName("blog_id")
    val blogId: String,
    @SerializedName("id")
    val id: String,
    @SerializedName("result")
    val result: String,
    @SerializedName("strtotime")
    val strtotime: String,
    @SerializedName("user_id")
    val userId: String,
    @SerializedName("comment")
    @Nullable
    val comment:String?

    )