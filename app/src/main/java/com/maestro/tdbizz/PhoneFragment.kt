package com.maestro.tdbizz

import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import com.maestro.tdbizz.databinding.FragmentPhoneBinding
import com.maestro.tdbizz.helper.Constant
import com.maestro.tdbizz.signup.SignUpResponse
import com.maestro.tdbizz.signup.SignUpViewModel
import com.maestro.tdbizz.ui.vendor.VendorsActivity
import io.paperdb.Paper


class PhoneFragment : Fragment() {
    lateinit var binding: FragmentPhoneBinding
    var otpValue: String? = null
    var signUpResponse: SignUpResponse? = null
    private val signUpViewModel: SignUpViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentPhoneBinding.inflate(layoutInflater)
        // Inflate the layout for this fragment


        Paper.init(context)
        signUpResponse = Paper.book().read(Constant.USER_DETAILS)






        binding.editOne.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (s!!.isNotEmpty() && s!!.length > 0) {

                    otpValue = s.toString()
                    binding.editTwo.requestFocus()
                }

            }

        })
        binding.editTwo.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

                if (s!!.isNotEmpty() && s!!.length > 0) {

                    otpValue = otpValue + s
                    binding.editThree.requestFocus()

                }
            }

        })

        binding.editThree.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (s!!.isNotEmpty() && s!!.length > 0) {
                    binding.editFour.requestFocus()
                    otpValue = otpValue + s

                }
            }

        })

        binding.editFour.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                otpValue = otpValue + s


                if (otpValue!!.length == 4) {


                    if (signUpResponse!!.otp.equals(otpValue)) {


                        if (requireActivity() is VendorsActivity) {
                            signUpViewModel.verifyOTP(
                                userID = signUpResponse!!.id,
                                otp = otpValue,
                                isVendor = true
                            )

                        } else {
                            signUpViewModel.verifyOTP(
                                userID = signUpResponse!!.id,
                                otp = otpValue,
                                isVendor = false
                            )

                        }


                    } else {

                        Toast.makeText(
                            context,
                            "Otp incorrect! Please try again",
                            Toast.LENGTH_LONG
                        ).show()

                        binding.editOne.setText("")

                        binding.editTwo.setText("")
                        binding.editThree.setText("")
                        binding.editFour.setText("")
                        otpValue = ""


                    }
                }


            }

        })



        binding.signInButton.setOnClickListener {
            if (otpValue!!.length == 4) {


                if (signUpResponse!!.otp.equals(otpValue)) {


                    if (requireActivity() is VendorsActivity) {
                        signUpViewModel.verifyOTP(
                            userID = signUpResponse!!.id,
                            otp = otpValue,
                            isVendor = true
                        )

                    } else {
                        signUpViewModel.verifyOTP(
                            userID = signUpResponse!!.id,
                            otp = otpValue,
                            isVendor = false
                        )

                    }
                } else {

                    Toast.makeText(context, "Otp incorrect! Please try again", Toast.LENGTH_LONG)
                        .show()

                    binding.editOne.setText("")

                    binding.editTwo.setText("")
                    binding.editThree.setText("")
                    binding.editFour.setText("")

                }
            } else {
                Toast.makeText(context, "Otp incorrect! Please try again", Toast.LENGTH_LONG).show()

            }


        }





        lifecycleScope.launchWhenStarted {


            signUpViewModel.signUpEvent.observe(viewLifecycleOwner, Observer { res ->


                when (res) {


                    is SignUpViewModel.SignUpEvent.SuccessOTP -> {

                        activity?.findViewById<ProgressBar>(R.id.loadingBar)?.isVisible = false


                        if (res.verifyOtpResponse.result) {
                            Toast.makeText(
                                context,
                                "Sign up successfull ",
                                Toast.LENGTH_SHORT
                            ).show()

                            startActivity(
                                Intent(
                                    context,
                                    MainActivity::class.java
                                ).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                            )

                        } else {

                            Toast.makeText(
                                context,
                                " Result ${res.verifyOtpResponse.message}",
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                    }

                    is SignUpViewModel.SignUpEvent.Failure -> {


                        activity?.findViewById<ProgressBar>(R.id.loadingBar)?.isVisible = false
                        Toast.makeText(context, "Sign up failed ${res.message}", Toast.LENGTH_SHORT)
                            .show()
                    }


                    is SignUpViewModel.SignUpEvent.Loading -> {


                        activity?.findViewById<ProgressBar>(R.id.loadingBar)?.isVisible = true
                    }
                }


            })


        }











        return binding.root
    }

}