package com.maestro.tdbizz.ui.newsletter.adapter

interface RefreshNestedComments {

    fun refresh(boolean: Boolean, commentID:String)

}