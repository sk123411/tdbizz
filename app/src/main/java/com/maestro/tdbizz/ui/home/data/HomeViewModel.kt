package com.maestro.tdbizz.ui.home.data

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.maestro.tdbizz.helper.Resource
import com.maestro.tdbizz.retrofit.RetrofitInstance
import com.maestro.tdbizz.ui.category.CategoryResponse
import com.maestro.tdbizz.ui.category.SubCategoryResponseItem
import com.maestro.tdbizz.ui.home.ProductResponse
import com.maestro.tdbizz.ui.home.ProductResponseItem
import com.maestro.tdbizz.ui.home.offer.OfferResponse
import com.maestro.tdbizz.ui.home.offer.OfferResponseItem

import kotlinx.coroutines.launch

class HomeViewModel : ViewModel() {


    private val homeEvent_ = MutableLiveData<HomeEvent>()
    var homeAPI: HomeAPI? = null
    var home: Home? = null


    val homeEvent: LiveData<HomeEvent>
        get() = homeEvent_


    init {
        homeAPI = RetrofitInstance.getRetrofitInstance().create(HomeAPI::class.java)
        home = HomeImplementation(homeAPI!!)


    }


    sealed class HomeEvent {

        class SuccessCategories(val categories: CategoryResponse) : HomeEvent()
        class SuccessProducts(val products: ProductResponse) : HomeEvent()
        class SuccessOffers(val offers: List<OfferResponseItem>) : HomeEvent()
        class SuccessProfile(val profile: ProfileResponse) : HomeEvent()
        class SuccessFeatureProducts(val products: List<ProductResponseItem>) : HomeEvent()

        class Failure(val message: String?) : HomeEvent()
        class SuccessSubCategories(val data: List<SubCategoryResponseItem>) :HomeEvent()
        object Loading : HomeEvent()
        object Empty : HomeEvent()


    }


    fun getCategories() {


        viewModelScope.launch {
            homeEvent_.value = HomeEvent.Loading

            val response = home!!.getCategory()

            when (response) {

                is Resource.Success -> {


                    homeEvent_.value = HomeEvent.SuccessCategories(response.data!!)


                }

                is Resource.Error -> {
                    homeEvent_.value = HomeEvent.Failure("Server error occured"!!)


                }


            }


        }


    }


    fun getFeatureProducts() {


        viewModelScope.launch {
            homeEvent_.value = HomeEvent.Loading

            val response = home!!.getFeatureProducts()

            when (response) {

                is Resource.Success -> {


            homeEvent_.value = HomeEvent.SuccessFeatureProducts(response.data!!)


                }

                is Resource.Error -> {
                    homeEvent_.value = HomeEvent.Failure("Server error occured"!!)


                }


            }


        }


    }


    fun getSubcategories(categoryID: String?) {

        val map = HashMap<String, String>()
        map.put("category_id", categoryID!!)


        viewModelScope.launch {
            homeEvent_.value = HomeEvent.Loading

            val response = home!!.getSubcategory(map)

            when (response) {

                is Resource.Success -> {


                    homeEvent_.value = HomeEvent.SuccessSubCategories(response.data!!)


                }

                is Resource.Error -> {
                    homeEvent_.value = HomeEvent.Failure("Server error occured"!!)


                }


            }


        }


    }


    fun getProducts() {


        viewModelScope.launch {
            homeEvent_.value = HomeEvent.Loading

            val response = home!!.getProducts()

            when (response) {

                is Resource.Success -> {


                    homeEvent_.value = HomeEvent.SuccessProducts(response.data!!)


                }

                is Resource.Error -> {
                    homeEvent_.value = HomeEvent.Failure("Server error occured"!!)


                }


            }


        }


    }

    fun getProfile(userId: String?) {


        val map = HashMap<String, String>()
        map.put("user_id", userId!!)




        viewModelScope.launch {
            homeEvent_.value = HomeEvent.Loading

            val response = home!!.getProfile(map)

            when (response) {

                is Resource.Success -> {


                    homeEvent_.value = HomeEvent.SuccessProfile(response.data!!)


                }

                is Resource.Error -> {
                    homeEvent_.value = HomeEvent.Failure("Server error occured"!!)


                }


            }


        }


    }


    fun updateProfile(
        userId: String?,
        firstName: String?,
        lastName: String?,
        mobile: String?,
        email: String?,
        gender: String?
    ) {
        val map = HashMap<String, String>()
        map.put("id", userId!!)
        map.put("first_name", firstName!!)
        map.put("last_name", lastName!!)
        map.put("mobile", mobile!!)
        map.put("email", email!!)
        map.put("gender", gender!!)




        viewModelScope.launch {
            homeEvent_.value = HomeEvent.Loading

            val response = home!!.updateProfile(map)

            when (response) {

                is Resource.Success -> {


                    homeEvent_.value = HomeEvent.SuccessProfile(response.data!!)


                }

                is Resource.Error -> {
                    homeEvent_.value = HomeEvent.Failure("Server error occured"!!)


                }


            }


        }


    }

    fun getOffers() {
        homeEvent_.value = HomeEvent.Loading

        viewModelScope.launch {

            val response = home!!.getOffers()

            when (response) {

                is Resource.Success -> {


                    homeEvent_.value = HomeEvent.SuccessOffers(response.data!!)


                }

                is Resource.Error -> {
                    homeEvent_.value = HomeEvent.Failure("Server error occured"!!)


                }


            }


        }

    }


}