package com.maestro.tdbizz.ui.product.data

import android.util.Log
import com.maestro.tdbizz.helper.Constant
import com.maestro.tdbizz.helper.Resource
import com.maestro.tdbizz.ui.home.ProductResponse
import com.maestro.tdbizz.ui.product.model.ProductAddedResponse

class ProductImplementation constructor(var productAPI: ProductAPI) : Product {
    override suspend fun getProductsByCategoryID(data: HashMap<String, String>): Resource<ProductResponse> {
        return try {

            val response = productAPI.getProducts(Constant.API_PRODUCT_BY_CATEGORY, data)


            val result = response.body()
            Log.d("XXXXXXXXX", "" + result.toString())

            if (response.isSuccessful && result != null) {
                Log.d("XXXXXXXXX", "" + result.toString())

                Resource.Success(result)


            } else {
                Resource.Error(response.message())
            }
        } catch (e: Exception) {
            Resource.Error(e.message ?: "An error occured")
        }
    }

    override suspend fun addProductToWishlist(data: HashMap<String, String>): Resource<ProductAddedResponse> {
        return try {

            val response = productAPI.addProductToWishlist(Constant.API_ADD_TO_WISHLIST, data)


            val result = response.body()
            Log.d("XXXXXXXXX", "" + result.toString())

            if (response.isSuccessful && result != null) {
                Log.d("XXXXXXXXX", "" + result.toString())

                Resource.Success(result)


            } else {
                Resource.Error(response.message())
            }
        } catch (e: Exception) {
            Resource.Error(e.message ?: "An error occured")
        }
    }

    override suspend fun showWishlist(data: HashMap<String, String>): Resource<ProductResponse> {
        return try {

            val response = productAPI.showWishlist(Constant.API_SHOW_WISHLIST,data)


            val result = response.body()
            Log.d("XXXXXXXXX", "" + result.toString())

            if (response.isSuccessful && result != null) {
                Log.d("XXXXXXXXX", "" + result.toString())

                Resource.Success(result)


            } else {
                Resource.Error(response.message())
            }
        } catch (e: Exception) {
            Resource.Error(e.message ?: "An error occured")
        }
    }


}