package com.maestro.tdbizz.ui.newsletter

import com.maestro.tdbizz.helper.Resource
import com.maestro.tdbizz.ui.newsletter.adapter.DeleteCommentResponse
import com.maestro.tdbizz.ui.newsletter.model.NewsAddViewResponse
import org.json.JSONObject

interface News {

    suspend fun showBlog(data:HashMap<String,String>):Resource<NewsResponse>
    suspend fun addCommentOnBlog(data:HashMap<String,String>):Resource<NewsLikeCommentAddedResponse>
    suspend fun addLikeOnBlog(data:HashMap<String,String>):Resource<NewsLikeCommentAddedResponse>
    suspend fun getCommentsForBLog(data:HashMap<String,String>):Resource<List<NewsLikeCommentAddedResponse>>
    suspend fun subscribeForBlog(data:HashMap<String,String>):Resource<SubscribeResponse>

    suspend fun getSubscribeStatus(data:HashMap<String,String>):Resource<SubscribeStatusResponse>
    suspend fun addReplyOnCommentOnBlog(data:HashMap<String,String>):Resource<NestedCommentResponse>
    suspend fun showRepliesOnCommentOnBlog(data:HashMap<String,String>):Resource<List<NestedCommentResponse>>
    suspend fun deleteCommentOnBlog(data:HashMap<String,String>):Resource<DeleteCommentResponse>

    suspend fun addViewOnBlog(data:HashMap<String,String>):Resource<NewsAddViewResponse>

}