package com.maestro.tdbizz.ui.home.offer

data class OfferResponseItem(
    val id: String,
    val image: String,
    val path: String,
    val title: String
)