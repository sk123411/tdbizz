package com.maestro.tdbizz.ui.newsletter


import com.google.gson.annotations.SerializedName

data class SubscribeStatusResponse(
    @SerializedName("Subsribe_status")
    val subsribeStatus: Int
)