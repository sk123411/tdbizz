package com.maestro.tdbizz.ui.order


import com.google.gson.annotations.SerializedName

data class CartUpdatedResponse(
    @SerializedName("color_id")
    val colorId: String,
    @SerializedName("grand_total")
    val grandTotal: String,
    @SerializedName("id")
    val id: String,
    @SerializedName("message")
    val message: String,
    @SerializedName("payment_date")
    val paymentDate: String,
    @SerializedName("payment_status")
    val paymentStatus: String,
    @SerializedName("payment_time")
    val paymentTime: String,
    @SerializedName("payment_type")
    val paymentType: String,
    @SerializedName("price")
    val price: String,
    @SerializedName("product_id")
    val productId: String,
    @SerializedName("quantity")
    val quantity: String,
    @SerializedName("shipping_address_id")
    val shippingAddressId: String,
    @SerializedName("size_id")
    val sizeId: String,
    @SerializedName("sub_total")
    val subTotal: String,
    @SerializedName("user_id")
    val userId: String,
    @SerializedName("vendor_id")
    val vendorId: String
)