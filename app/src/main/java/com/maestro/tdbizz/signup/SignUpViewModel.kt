package com.maestro.tdbizz.signup

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.maestro.tdbizz.helper.Resource
import com.maestro.tdbizz.retrofit.RetrofitInstance
import com.maestro.tdbizz.signup.SignUpResponse
import com.maestro.tdbizz.signup.VerifyOtpResponse

import kotlinx.coroutines.launch
import retrofit2.Retrofit

class SignUpViewModel : ViewModel() {


    private val signUpEvent_ = MutableLiveData<SignUpEvent>()
    var signUpAPI: SignUpAPI
    var signUp: Signup


    val signUpEvent: LiveData<SignUpEvent>
        get() = signUpEvent_


    init {
        signUpAPI = RetrofitInstance.getRetrofitInstance().create(SignUpAPI::class.java)
        signUp = SignUpImplementation(signUpAPI)


    }


    sealed class SignUpEvent {

        class Success(val signUpResponse: SignUpResponse) : SignUpEvent()
        class SuccessOTP(val verifyOtpResponse: VerifyOtpResponse) : SignUpEvent()

        class Failure(val message: String?) : SignUpEvent()
        object Loading : SignUpEvent()
        object Empty : SignUpEvent()

    }


    fun register(email: String?, mobile: String?, isVendor: Boolean?) {

        val data = HashMap<String, Any>()
        data.put("email", email!!)
        data.put("mobile", mobile!!)

        if (isVendor!!) {

            data.put("isVendor", 1)

        } else {
            data.put("isVendor", 0)

        }



        viewModelScope.launch {
            signUpEvent_.value = SignUpEvent.Loading

            val response = signUp.register(data)

            when (response) {

                is Resource.Success -> {


                    signUpEvent_.value = SignUpEvent.Success(response.data!!)


                }

                is Resource.Error -> {
                    signUpEvent_.value = SignUpEvent.Failure("Sign up successfull"!!)


                }


            }


        }


    }

    fun login(mobile: String?, isVendor: Boolean?) {

        val data = HashMap<String, Any>()
        data.put("mobile", mobile!!)
        if (isVendor!!) {

            data.put("isVendor", 1)

        } else {
            data.put("isVendor", 0)

        }

        viewModelScope.launch {
            signUpEvent_.value = SignUpEvent.Loading

            val response = signUp.login(data)

            when (response) {

                is Resource.Success -> {


                    signUpEvent_.value = SignUpEvent.Success(response.data!!)


                }

                is Resource.Error -> {
                    signUpEvent_.value = SignUpEvent.Failure("Sign in failed")


                }


            }


        }


    }

    fun verifyOTP(userID: String?, otp: String?,isVendor: Boolean?) {

        val data = HashMap<String, Any>()
        data.put("user_id", userID!!)
        data.put("otp", otp!!)
        if (isVendor!!) {

            data.put("isVendor", 1)

        } else {
            data.put("isVendor", 0)

        }

        viewModelScope.launch {
            signUpEvent_.value = SignUpEvent.Loading

            val response = signUp.verifyOTP(data)

            when (response) {

                is Resource.Success -> {


                    signUpEvent_.value = SignUpEvent.SuccessOTP(response.data!!)


                }

                is Resource.Error -> {
                    signUpEvent_.value = SignUpEvent.Failure("Sign in failed")


                }


            }


        }


    }


}