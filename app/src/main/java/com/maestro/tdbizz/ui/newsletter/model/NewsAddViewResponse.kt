package com.maestro.tdbizz.ui.newsletter.model


import com.google.gson.annotations.SerializedName

data class NewsAddViewResponse(
    @SerializedName("blog_id")
    val blogId: String?,
    @SerializedName("id")
    val id: String?,
    @SerializedName("result")
    val result: String?,
    @SerializedName("user_id")
    val userId: String?

)