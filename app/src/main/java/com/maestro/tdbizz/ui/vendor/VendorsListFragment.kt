package com.maestro.tdbizz.ui.vendor

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.maestro.tdbizz.R
import com.maestro.tdbizz.databinding.FragmentVendorsListBinding
import com.maestro.tdbizz.ui.category.CategorySubcategoryAdapter
import com.maestro.tdbizz.ui.category.SubCategoryResponseItem
import com.maestro.tdbizz.ui.home.data.Home
import com.maestro.tdbizz.ui.home.data.HomeViewModel
import com.maestro.tdbizz.ui.vendor.data.VendorViewModel


class VendorsListFragment : Fragment() {
    lateinit var binding: FragmentVendorsListBinding
    private val vendorViewModel: VendorViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentVendorsListBinding.inflate(layoutInflater)
        // Inflate the layout for this fragment
        vendorViewModel.getAllVendors()









        lifecycleScope.launchWhenStarted {












            vendorViewModel.vendorEvent.observe(viewLifecycleOwner, Observer {

                when (it) {


                    is VendorViewModel.HomeEvent.SuccessVendors -> {

                        activity?.findViewById<ProgressBar>(R.id.loadingBar)?.isVisible = false

                        if (it.vendors.isNotEmpty()) {


                            binding.vendorsList.apply {


                                layoutManager = GridLayoutManager(context, 2)
                                adapter =
                                    VendorAdapter(it.vendors)


                            }
                        }


                    }
                    is VendorViewModel.HomeEvent.Loading -> {
                        activity?.findViewById<ProgressBar>(R.id.loadingBar)?.isVisible = true


                    }
                }


            })

        }







        return binding.root
    }

}