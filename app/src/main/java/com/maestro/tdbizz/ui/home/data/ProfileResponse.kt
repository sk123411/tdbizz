package com.maestro.tdbizz.ui.home.data


import com.google.gson.annotations.SerializedName

data class ProfileResponse(
    @SerializedName("additional_info")
    val additionalInfo: String,
    @SerializedName("address")
    val address: String,
    @SerializedName("area")
    val area: String,
    @SerializedName("email")
    val email: String,
    @SerializedName("first_name")
    val firstName: String,
    @SerializedName("gender")
    val gender: String,
    @SerializedName("id")
    val id: String,
    @SerializedName("last_name")
    val lastName: String,
    @SerializedName("mobile")
    val mobile: String,
    @SerializedName("mobile2")
    val mobile2: String,
    @SerializedName("otp")
    val otp: String,
    @SerializedName("password")
    val password: String,
    @SerializedName("region")
    val region: String,
    @SerializedName("register_agree")
    val registerAgree: String
)