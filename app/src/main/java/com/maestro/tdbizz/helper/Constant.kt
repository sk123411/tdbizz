package com.maestro.tdbizz.helper

import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.net.Uri
import com.bumptech.glide.Glide
import com.maestro.tdbizz.signup.SignUpResponse
import com.squareup.picasso.Picasso
import com.squareup.picasso.Target
import io.paperdb.Paper
import java.io.File
import java.io.FileOutputStream
import java.io.IOException

class Constant  {



companion object {
    val API_ADD_VIEW: String="add_view_blog_count"
    val API_DELETE_COMMENT: String="delete_comment"
    val API_DELETE_REPLY_COMMENT: String="delete_comment_oncomment"
    val API_SHOW_REPLY_COMMENT: String="show_comment_oncomment"
    val API_ADD_REPLY_COMMENT: String="add_comment_oncomment"
    val PRODUCTS: String?="products"
    val API_GET_OFFERS: String?="show_offers"
    val API_DELETE_FROM_CART: String?="delete_product_from_cart"
    val API_UPDATE_CART: String?="update_cart"
    val API_GET_CARTS: String?="show_cart"
    val API_ADD_TO_CART: String?="add_to_cart"
    val API_FEATURE_PRODUCTS: String?="get_featured_product"
    val API_SUBSCRIBE_STATUS: String="get_subscribe_status"
    val API_ADD_BLOG_SUBSCRIBE: String="subscribe_news_letter"
    val API_SHOW_COMMENT: String="show_blog_comment"
    val API_ADD_COMMENT: String="add_blog_comment"
    val API_ADD_LIKE: String="blog_like"
    val API_GET_ALL_BLOGS: String?="show_blog"
    val API_CHANGE_VENDOR_DETAILS: String?="change_vendor_detail"
    val API_GET_VENDOR_PRODUCTS: String?="vendor_product"
    val API_GET_VENDOR_DETAIL: String?="vendor_detail"
    val API_ADD_PRODUCT: String? ="add_product"
    val API_GET_ALL_VENDOR: String?="get_all_vendors"
    val API_SUB_CATEGORY: String?="get_subcategory"
    val PRODUCT_DETAILS: String?="product_details"
    val API_ADD_TO_WISHLIST: String?="add_wishlist"
    val API_SHOW_WISHLIST: String?="show_wishlist"

    val API_PRODUCT_BY_CATEGORY: String?="product_by_subcategory"
    val API_UPDATE_PROFILE: String?="update_profile"
    val API_PROFILE: String?="show_profile"
    val API_CATEGORY: String?="category"
    val API_PRODUCT: String?="product"

    val API_OTP_VERIFY: String?="otp_verify"
    val USER_DETAILS: String?="user_details"
    val API_LOGIN: String?="login"
    const val API_SIGNUP="signup"


    const val BASE_URL_IMAGE="https://ruparnatechnology.com/TDbizz/Admin/image/"


    fun getUserID(context:Context):SignUpResponse{

        Paper.init(context)
        return Paper.book().read(Constant.USER_DETAILS)

    }



    fun shareImageFromURI(url: String?, title: String?, context: Context) {
        Picasso.get().load(url).into(object : Target {
            override fun onBitmapLoaded(bitmap: Bitmap?, from: Picasso.LoadedFrom?) {

                val intent = Intent(Intent.ACTION_SEND)
                intent.type = "*/*"
                intent.putExtra(Intent.EXTRA_STREAM, getBitmapFromView(bitmap, context))
                intent.putExtra(
                    Intent.EXTRA_TEXT,
                    title + "\n" + "Dresspanda download app from playstore"
                )
                context.startActivity(Intent.createChooser(intent, "Share Image"))
            }

            override fun onPrepareLoad(placeHolderDrawable: Drawable?) {}
            override fun onBitmapFailed(e: java.lang.Exception?, errorDrawable: Drawable?) {}
        })
    }


    fun getBitmapFromView(bitmap: Bitmap?, context: Context?): Uri? {
        var bmpUri: Uri? = null
        try {
            val file =
                File(
                    context!!.externalCacheDir,
                    System.currentTimeMillis().toString() + ".jpg"
                )

            val out = FileOutputStream(file)
            bitmap?.compress(Bitmap.CompressFormat.JPEG, 90, out)
            out.close()
            bmpUri = Uri.fromFile(file)

        } catch (e: IOException) {
            e.printStackTrace()
        }

        return bmpUri!!

    }


}

}