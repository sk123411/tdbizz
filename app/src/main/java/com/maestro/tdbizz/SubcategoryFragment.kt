package com.maestro.tdbizz

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.GridLayoutManager
import com.maestro.tdbizz.databinding.FragmentSubcategoryBinding
import com.maestro.tdbizz.ui.category.CategorySubcategoryAdapter
import com.maestro.tdbizz.ui.category.SubCategoryResponseItem
import com.maestro.tdbizz.ui.home.data.HomeViewModel

class SubcategoryFragment : Fragment() {
    lateinit var binding: FragmentSubcategoryBinding
    private val homeViewModel: HomeViewModel by viewModels()


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment

        binding = FragmentSubcategoryBinding.inflate(layoutInflater)


        val id = arguments?.getString("id")

        if (id != null) {
            homeViewModel.getSubcategories(id)
        }



        lifecycleScope.launchWhenStarted {


            homeViewModel.homeEvent.observe(viewLifecycleOwner, Observer {

                when (it) {


                    is HomeViewModel.HomeEvent.SuccessSubCategories -> {


                        if (it.data.isNotEmpty()) {

                            activity?.findViewById<ProgressBar>(R.id.loadingBar)?.isVisible = false

                            binding.categoriesList.apply {


                                layoutManager = GridLayoutManager(context, 2)
                                adapter =
                                    CategorySubcategoryAdapter<SubCategoryResponseItem>(it.data)

                            }
                        }


                    }
                    is HomeViewModel.HomeEvent.Loading -> {
                        activity?.findViewById<ProgressBar>(R.id.loadingBar)?.isVisible = true


                    }
                }


            })

        }



        return binding.root
    }


}