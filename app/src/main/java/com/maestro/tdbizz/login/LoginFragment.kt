package com.maestro.tdbizz.login

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import androidx.navigation.Navigation
import com.maestro.tdbizz.R
import com.maestro.tdbizz.ui.vendor.VendorsActivity
import com.maestro.tdbizz.databinding.FragmentLoginBinding
import com.maestro.tdbizz.helper.Constant
import com.maestro.tdbizz.signup.SignUpViewModel

import io.paperdb.Paper


class LoginFragment : Fragment() {


    lateinit var binding: FragmentLoginBinding
    private val signUpViewModel: SignUpViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment

        binding = FragmentLoginBinding.inflate(layoutInflater)







        lifecycleScope.launchWhenStarted {


            signUpViewModel.signUpEvent.observe(viewLifecycleOwner, Observer {


                when (it) {

                    is SignUpViewModel.SignUpEvent.Success -> {

                        activity?.findViewById<ProgressBar>(R.id.loadingBar)?.isVisible = false



                        if (it.signUpResponse.result.toLowerCase().contains("login")) {

                            Paper.init(context)
                            Paper.book().write(Constant.USER_DETAILS, it.signUpResponse)

                            Navigation.findNavController(requireView()).navigate(R.id.phoneFragment)
                        } else {

                            Toast.makeText(
                                context,
                                " Result ${it.signUpResponse.result}",
                                Toast.LENGTH_SHORT
                            ).show()

                        }


                    }


                    is SignUpViewModel.SignUpEvent.Failure -> {
                        activity?.findViewById<ProgressBar>(R.id.loadingBar)?.isVisible = false

                    }
                    is SignUpViewModel.SignUpEvent.Loading -> {

                        activity?.findViewById<ProgressBar>(R.id.loadingBar)?.isVisible = true

                    }


                }


            })


        }



        binding.signInButton.setOnClickListener {


            if (requireActivity() is VendorsActivity) {
                signUpViewModel.login(binding.mobileEdit.text.toString(), true)

            } else {

                if (binding.mobileEdit.text.toString().isNotEmpty()) {
                    signUpViewModel.login(binding.mobileEdit.text.toString(), false)

                }
            }

        }




        return binding.root


    }

}