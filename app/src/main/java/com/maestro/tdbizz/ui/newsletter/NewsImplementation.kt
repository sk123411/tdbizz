package com.maestro.tdbizz.ui.newsletter

import android.util.Log
import com.maestro.tdbizz.helper.Constant
import com.maestro.tdbizz.helper.Resource
import com.maestro.tdbizz.ui.newsletter.adapter.DeleteCommentResponse
import com.maestro.tdbizz.ui.newsletter.model.NewsAddViewResponse
import org.json.JSONObject


class NewsImplementation (val newsAPI: NewsAPI):News{
    override suspend fun showBlog(data: HashMap<String, String>): Resource<NewsResponse> {
        return try {

            val response = newsAPI.showBlog(Constant.API_GET_ALL_BLOGS!!,data)


            val result = response.body()
            Log.d("", "" + result.toString())

            if (response.isSuccessful && result != null) {
                Log.d("XXXXXXXXX", "" + result.toString())

                Resource.Success(result)


            } else {
                Resource.Error(response.message())
            }
        } catch (e: Exception) {
            Resource.Error(e.message ?: "An error occured")
        }


    }

    override suspend fun addCommentOnBlog(data: HashMap<String, String>): Resource<NewsLikeCommentAddedResponse> {
        return try {

            val response = newsAPI.addCommentOnBlog(Constant.API_ADD_COMMENT,data)


            val result = response.body()
            Log.d("XXXXXXXXX", "" + result.toString())

            if (response.isSuccessful && result != null) {
                Log.d("XXXXXXXXX", "" + result.toString())

                Resource.Success(result)


            } else {
                Resource.Error(response.message())
            }
        } catch (e: Exception) {
            Resource.Error(e.message ?: "An error occured")
        }
    }

    override suspend fun addLikeOnBlog(data: HashMap<String, String>): Resource<NewsLikeCommentAddedResponse> {
        return try {

            val response = newsAPI.addLikeOnBlog(Constant.API_ADD_LIKE,data)


            val result = response.body()
            Log.d("XXXXXXXXX", "" + result.toString())

            if (response.isSuccessful && result != null) {
                Log.d("XXXXXXXXX", "" + result.toString())

                Resource.Success(result)


            } else {
                Resource.Error(response.message())
            }
        } catch (e: Exception) {
            Resource.Error(e.message ?: "An error occured")
        }

    }

    override suspend fun getCommentsForBLog(data: HashMap<String, String>): Resource<List<NewsLikeCommentAddedResponse>> {
        return try {

            val response = newsAPI.getCommentsForBLog(Constant.API_SHOW_COMMENT,data)


            val result = response.body()
            Log.d("XXXXXXXXX", "" + result.toString())

            if (response.isSuccessful && result != null) {
                Log.d("XXXXXXXXX", "" + result.toString())

                Resource.Success(result)


            } else {
                Resource.Error(response.message())
            }
        } catch (e: Exception) {
            Resource.Error(e.message ?: "An error occured")
        }
    }

    override suspend fun subscribeForBlog(data: HashMap<String, String>): Resource<SubscribeResponse> {
        return try {

            val response = newsAPI.subscribeForBlog(Constant.API_ADD_BLOG_SUBSCRIBE,data)


            val result = response.body()
            Log.d("XXXXXXXXX", "" + result.toString())
            Log.d("XXXXXXXXXPARAMS", "" + data.toString())

            if (response.isSuccessful && result != null) {
                Log.d("XXXXXXXXX", "" + result.toString())

                Resource.Success(result)


            } else {
                Resource.Error(response.message())
            }
        } catch (e: Exception) {
            Resource.Error(e.message ?: "An error occured")
        }    }

    override suspend fun getSubscribeStatus(data: HashMap<String, String>): Resource<SubscribeStatusResponse> {
        return try {

            val response = newsAPI.getSubscribeStatus(Constant.API_SUBSCRIBE_STATUS,data)


            val result = response.body()
            Log.d("XXXXXXXXX", "" + result.toString())
            Log.d("XXXXXXXXXPARAMS", "" + data.toString())

            if (response.isSuccessful && result != null) {
                Log.d("XXXXXXXXX", "" + result.toString())

                Resource.Success(result)


            } else {
                Resource.Error(response.message())
            }
        } catch (e: Exception) {
            Resource.Error(e.message ?: "An error occured")
        }


    }

    override suspend fun addReplyOnCommentOnBlog(data: HashMap<String, String>): Resource<NestedCommentResponse> {
        return try {

            val response = newsAPI.addReplyOnCommentOnBlog(Constant.API_ADD_REPLY_COMMENT,data)


            val result = response.body()
            Log.d("XXXXXXXXX", "" + result.toString())
            Log.d("XXXXXXXXXPARAMS", "" + data.toString())

            if (response.isSuccessful && result != null) {
                Log.d("XXXXXXXXX", "" + result.toString())

                Resource.Success(result)


            } else {
                Resource.Error(response.message())
            }
        } catch (e: Exception) {
            Resource.Error(e.message ?: "An error occured")
        }
    }

    override suspend fun showRepliesOnCommentOnBlog(data: HashMap<String, String>): Resource<List<NestedCommentResponse>> {
        return try {

            val response = newsAPI.showRepliesOnCommentOnBlog(Constant.API_SHOW_REPLY_COMMENT,data)


            val result = response.body()
            Log.d("XXXXXXXXX", "" + result.toString())
            Log.d("XXXXXXXXXPARAMS", "" + data.toString())

            if (response.isSuccessful && result != null) {
                Log.d("XXXXXXXXX", "" + result.toString())

                Resource.Success(result)


            } else {
                Resource.Error(response.message())
            }
        } catch (e: Exception) {
            Resource.Error(e.message ?: "An error occured")
        }
    }

    override suspend fun deleteCommentOnBlog(data: HashMap<String, String>): Resource<DeleteCommentResponse> {
        return try {

            val response = newsAPI.deleteCommentOnBlog(Constant.API_DELETE_COMMENT,data)

            val result = response.body()
            Log.d("XXXXXXXXX", "" + result.toString())
            Log.d("XXXXXXXXXPARAMS", "" + data.toString())

            if (response.isSuccessful && result != null) {
                Log.d("XXXXXXXXX", "" + result.toString())

                Resource.Success(result)


            } else {
                Resource.Error(response.message())
            }
        } catch (e: Exception) {
            Resource.Error(e.message ?: "An error occured")
        }
    }

    override suspend fun addViewOnBlog(data: HashMap<String, String>): Resource<NewsAddViewResponse> {
        return try {

            val response = newsAPI.addViewOnBlog(Constant.API_ADD_VIEW,data)

            val result = response.body()
            Log.d("XXXXXXXXX", "" + result.toString())
            Log.d("XXXXXXXXXPARAMS", "" + data.toString())

            if (response.isSuccessful && result != null) {
                Log.d("XXXXXXXXX", "" + result.toString())

                Resource.Success(result)


            } else {
                Resource.Error(response.message())
            }
        } catch (e: Exception) {
            Resource.Error(e.message ?: "An error occured")
        }
    }

}