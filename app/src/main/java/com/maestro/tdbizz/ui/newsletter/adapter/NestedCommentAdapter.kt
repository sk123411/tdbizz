package com.maestro.tdbizz.ui.newsletter.adapter

import android.content.DialogInterface
import android.util.Log
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.LinearLayoutCompat
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.observe
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.androidnetworking.AndroidNetworking
import com.androidnetworking.error.ANError
import com.androidnetworking.interfaces.ParsedRequestListener
import com.maestro.tdbizz.databinding.CommentItemLytBinding
import com.maestro.tdbizz.databinding.NestedCommentItemLytBinding
import com.maestro.tdbizz.helper.Constant
import com.maestro.tdbizz.ui.newsletter.*
import java.util.*


class NestedCommentAdapter(

    val newsFragment: NewsFragment,
    val commentID: String,
    val list: MutableList<NestedCommentResponse>, val size: Int,
    val activity: FragmentActivity
) :
    RecyclerView.Adapter<NestedCommentAdapter.MyViewholder>() {
    class MyViewholder(view: View) : RecyclerView.ViewHolder(view) {

        val commentItemLytBinding = NestedCommentItemLytBinding.bind(view)
        fun bindData(
            newsFragment: NewsFragment,
            data: NestedCommentResponse,
            commentID: String,
            activity: FragmentActivity) {

            if (data.userId.equals(
                    Constant.getUserID(
                        context =
                        commentItemLytBinding.root.context
                    ).id
                )
            ) {
                val lp =
                    LinearLayoutCompat.LayoutParams(
                        LinearLayout.LayoutParams.WRAP_CONTENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT
                    )
                lp.weight = 1.0f;
                lp.gravity = Gravity.TOP;

                commentItemLytBinding.commentRootLayout.layoutParams = lp

                commentItemLytBinding.deleteButton.visibility = View.VISIBLE

            } else {
                commentItemLytBinding.deleteButton.visibility = View.GONE

            }
            commentItemLytBinding.userName.text = data.id
            commentItemLytBinding.userComment.text = data.comment


            //  commentItemLytBinding.userLabel.text = data.userId.substring(0,1)


            commentItemLytBinding.deleteButton.setOnClickListener {

                val alertDialog = AlertDialog.Builder(activity)
                alertDialog.setTitle("Delete comment!")
                alertDialog.setMessage("Do you want to delete this comment?")
                alertDialog.setPositiveButton("Yes", object : DialogInterface.OnClickListener {
                    override fun onClick(dialog: DialogInterface?, which: Int) {

                        AndroidNetworking.post("https://ruparnatechnology.com/TDbizz/api/process.php?action=delete_comment_oncomment")
                            .addBodyParameter("user_id", Constant.getUserID(itemView.context).id)
                            .addBodyParameter("comment_id", commentID)
                            .addBodyParameter("id", data.id)
                            .build().getAsObject(
                                NestedCommentResponse::class.java,
                                object : ParsedRequestListener<NestedCommentResponse> {
                                    override fun onResponse(response: NestedCommentResponse?) {




                                        NewsCommentAdapter.refreshNestedComments!!.refresh(
                                            true, data.id
                                        )


                                    }

                                    override fun onError(anError: ANError?) {
                                    }
                                })

                    }

                }).setNegativeButton("No", object : DialogInterface.OnClickListener {
                    override fun onClick(dialog: DialogInterface?, which: Int) {
                        dialog!!.dismiss()
                    }

                }).show()


            }


        }


    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewholder {

        val binding = NestedCommentItemLytBinding.inflate(LayoutInflater.from(parent.context))
        return MyViewholder(binding.root)
    }

    override fun getItemCount(): Int {

        try {


            return size


        } catch (exception: IndexOutOfBoundsException) {

            return 0


        }
    }

    override fun onBindViewHolder(holder: MyViewholder, position: Int) {


        holder.bindData(
            newsFragment,list.get(position), commentID, activity
        )


//        if (size<=2) {
//            NewsCommentAdapter.nestedToParsedRequestListener!!.visibleShowAllButton(true)
//        }else {
//            NewsCommentAdapter.nestedToParsedRequestListener!!.visibleShowAllButton(false)
//
//        }
    }


}