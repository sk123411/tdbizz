package com.maestro.tdbizz.ui.product.model


import com.google.gson.annotations.SerializedName

data class ProductAddedResponse(
    @SerializedName("id")
    val id: String,
    @SerializedName("product_id")
    val productId: String,
    @SerializedName("result")
    val result: String,
    @SerializedName("user_id")
    val userId: String
)