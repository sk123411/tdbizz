package com.maestro.tdbizz.ui.vendor

import android.app.AlertDialog
import android.content.DialogInterface
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import androidx.navigation.Navigation
import androidx.recyclerview.widget.GridLayoutManager
import com.maestro.tdbizz.R
import com.maestro.tdbizz.databinding.FragmentVendorStoreBinding
import com.maestro.tdbizz.helper.Constant
import com.maestro.tdbizz.ui.product.ProductAdapter
import com.maestro.tdbizz.ui.vendor.data.VendorViewModel

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [VendorStoreFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class VendorStoreFragment : Fragment() {

    lateinit var binding:FragmentVendorStoreBinding


    private val vendorViewModel:VendorViewModel by viewModels()
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentVendorStoreBinding.inflate(layoutInflater)




        val isFromVendorList = requireArguments().getString("id")

        if (isFromVendorList!=null){

            if (!isFromVendorList.equals("")) {
                vendorViewModel.getVendorDetails(isFromVendorList)
                vendorViewModel.getVendorProducts(isFromVendorList)
            }

        }else {

            vendorViewModel.getVendorDetails(Constant.getUserID(requireContext()).id)
            vendorViewModel.getVendorProducts(Constant.getUserID(requireContext()).id)

        }






        lifecycleScope.launchWhenStarted {



            vendorViewModel.vendorEvent.observe(viewLifecycleOwner, Observer {

                when(it){


                    is VendorViewModel.HomeEvent.SuccessVendorDetail -> {

                        activity?.findViewById<ProgressBar>(R.id.loadingBar)?.isVisible = false

                        binding.vendorName.text = it.vendors.name
                        binding.vendorStoreName.text = it.vendors.storeName

                    }

                    is VendorViewModel.HomeEvent.SuccessVendorProducts -> {

                        activity?.findViewById<ProgressBar>(R.id.loadingBar)?.isVisible = false

                        binding.vendorProductList.apply {

                            layoutManager = GridLayoutManager(context, 2)
                            adapter = ProductAdapter(it.productResponse)



                        }

                        if (it.productResponse.size<1){



                            openEmptyAlertDialog(requireActivity())


                        }




                    }
                    is VendorViewModel.HomeEvent.Loading-> {
                        activity?.findViewById<ProgressBar>(R.id.loadingBar)?.isVisible = true



                    }

                }


            })

        }












        return binding.root
    }

    private fun openEmptyAlertDialog(activity: FragmentActivity) {


        val alertDialog = AlertDialog.Builder(activity)
            .setTitle("Sorry!!")
            .setMessage("No products available from this vendor")
            .setPositiveButton("Back", object :DialogInterface.OnClickListener{
                override fun onClick(dialog: DialogInterface?, which: Int) {


                    Navigation.findNavController(requireView()).navigateUp()



                }

            })
        alertDialog.create().show()






    }


}