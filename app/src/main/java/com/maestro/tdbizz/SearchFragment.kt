package com.maestro.tdbizz

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.maestro.tdbizz.databinding.FragmentHomeBinding
import com.maestro.tdbizz.databinding.FragmentSearchBinding
import com.maestro.tdbizz.helper.Constant
import com.maestro.tdbizz.ui.home.ProductResponseItem
import com.maestro.tdbizz.ui.product.ProductAdapter
import io.paperdb.Paper


class SearchFragment : Fragment() {


    lateinit var binding:FragmentSearchBinding



    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentSearchBinding.inflate(layoutInflater)

        Paper.init(context)


        binding.editSearch.addTextChangedListener(object :TextWatcher{
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

                val matchedList = mutableListOf<ProductResponseItem>()

                val savedList = Paper.book().read<List<ProductResponseItem>>(Constant.PRODUCTS)

                Log.d("PRRRRRRRRRR", "::"+savedList.toString())



                    for (item in savedList){

                        if (item.name.toLowerCase().contains(s.toString().toLowerCase())){
                            matchedList.add(item)
                        }

                    }


                    binding.searchList.apply {

                        Log.d("PRRRRRRRRRRM", "::"+matchedList.toString())

                        layoutManager = GridLayoutManager(context,
                            2)
                        adapter =
                            ProductAdapter(matchedList)

                    }






            }

            override fun afterTextChanged(s: Editable?) {
            }

        })




        return binding.root
    }

}