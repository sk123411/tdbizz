package com.maestro.tdbizz.ui.vendor.data

import com.maestro.tdbizz.helper.Resource
import com.maestro.tdbizz.ui.category.CategoryResponse
import com.maestro.tdbizz.ui.category.SubCategoryResponseItem
import com.maestro.tdbizz.ui.home.ProductResponse
import com.maestro.tdbizz.ui.home.ProductResponseItem
import com.maestro.tdbizz.ui.vendor.model.VendorDetailResponse
import com.maestro.tdbizz.ui.vendor.model.VendorResponse
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.http.POST

interface Vendor {

    suspend fun getAllVendors(): Resource<List<VendorResponse>>

    suspend fun addProduct(
        image: MultipartBody.Part?,
        name: RequestBody?,
        desc: RequestBody?,
        price: RequestBody?,
        brand: RequestBody?,
        size: RequestBody?,
        color: RequestBody?,
        category_id: RequestBody?,
        subcategory_id: RequestBody?,
        arrival_status: RequestBody?,
        vendor_id: RequestBody?

    ): Resource<ProductResponseItem>


    suspend fun getVendorDetails(data: HashMap<String, String>): Resource<VendorDetailResponse>
    suspend fun getVendorProducts(data: HashMap<String, String>): Resource<ProductResponse>

    suspend fun changeVendorDetails(data: HashMap<String, String>): Resource<VendorDetailResponse>
    suspend fun changeVendorStoreImage(
        image: MultipartBody.Part?, id: RequestBody?
        ): Resource<VendorDetailResponse>

}