package com.maestro.tdbizz.ui.product.data

import com.maestro.tdbizz.helper.Resource
import com.maestro.tdbizz.ui.home.ProductResponse
import com.maestro.tdbizz.ui.product.model.ProductAddedResponse

interface Product {

    suspend fun getProductsByCategoryID(data: HashMap<String,String>): Resource<ProductResponse>
    suspend fun addProductToWishlist(data: HashMap<String,String>): Resource<ProductAddedResponse>
    suspend fun showWishlist(data: HashMap<String,String>): Resource<ProductResponse>

}