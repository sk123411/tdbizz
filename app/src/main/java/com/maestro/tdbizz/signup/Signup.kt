package com.maestro.tdbizz.signup

import com.maestro.tdbizz.helper.Resource
import com.maestro.tdbizz.signup.SignUpResponse
import com.maestro.tdbizz.signup.VerifyOtpResponse
import retrofit2.http.FieldMap
import retrofit2.http.Query

interface Signup {

    suspend fun register(data:HashMap<String,Any>): Resource<SignUpResponse>
    suspend fun login(data:HashMap<String,Any>): Resource<SignUpResponse>
    suspend fun verifyOTP(data:HashMap<String,Any>): Resource<VerifyOtpResponse>

}