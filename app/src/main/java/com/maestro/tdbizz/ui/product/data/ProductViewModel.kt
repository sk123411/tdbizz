package com.maestro.tdbizz.ui.product.data

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.maestro.tdbizz.helper.Resource
import com.maestro.tdbizz.retrofit.RetrofitInstance
import com.maestro.tdbizz.ui.category.CategoryResponse
import com.maestro.tdbizz.ui.home.ProductResponse
import com.maestro.tdbizz.ui.product.model.ProductAddedResponse

import kotlinx.coroutines.launch

class ProductViewModel :ViewModel(){



    private val productEvent_ = MutableLiveData<ProductEvent>()
    var productAPI: ProductAPI?=null
    var product: Product?=null



    val productEvent:LiveData<ProductEvent>
        get() = productEvent_


init {
    productAPI = RetrofitInstance.getRetrofitInstance().create(ProductAPI::class.java)
    product = ProductImplementation(productAPI!!)


}






    sealed class ProductEvent{

        class SuccessCategories(val categories: CategoryResponse):ProductEvent()
        class SuccessProducts(val products: ProductResponse):ProductEvent()
        class SuccessProductAddedToWishlistt(val products: ProductAddedResponse):ProductEvent()

        class Failure(val message:String?):ProductEvent()
         object Loading:ProductEvent()
         object Empty:ProductEvent()

    }





    fun getProducts(categoryID:String?){

        val map = HashMap<String,String>()
        map.put("subcategory_id",categoryID!!)




        viewModelScope.launch {
            productEvent_.value = ProductEvent.Loading

            val response = product!!.getProductsByCategoryID(map)

            when (response) {

                is Resource.Success -> {


                    productEvent_.value = ProductEvent.SuccessProducts(response.data!!)



                }

                is Resource.Error -> {
                    productEvent_.value = ProductEvent.Failure("Server error occured"!!)


                }


            }



        }



    }




    fun addProductToWishlist(productID:String?,userID:String?){

        val map = HashMap<String,String>()
        map.put("product_id",productID!!)
        map.put("user_id",userID!!)

        productEvent_.value = ProductEvent.Loading



        viewModelScope.launch {

            val response = product!!.addProductToWishlist(map)

            when (response) {

                is Resource.Success -> {


                    productEvent_.value = ProductEvent.SuccessProductAddedToWishlistt(response.data!!)



                }

                is Resource.Error -> {
                    productEvent_.value = ProductEvent.Failure("Server error occured"!!)


                }


            }



        }



    }

    fun showWishlist(userID:String?){

        val map = HashMap<String,String>()
        map.put("user_id",userID!!)
        productEvent_.value = ProductEvent.Loading




        viewModelScope.launch {

            val response = product!!.showWishlist(map)

            when (response) {

                is Resource.Success -> {


                    productEvent_.value = ProductEvent.SuccessProducts(response.data!!)



                }

                is Resource.Error -> {
                    productEvent_.value = ProductEvent.Failure("Server error occured"!!)


                }


            }



        }



    }



}