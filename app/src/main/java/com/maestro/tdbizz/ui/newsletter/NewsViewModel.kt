package com.maestro.tdbizz.ui.newsletter

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.maestro.tdbizz.helper.Resource
import com.maestro.tdbizz.retrofit.RetrofitInstance
import com.maestro.tdbizz.ui.newsletter.adapter.DeleteCommentResponse
import com.maestro.tdbizz.ui.newsletter.model.NewsAddViewResponse

import kotlinx.coroutines.launch
import org.json.JSONObject

class NewsViewModel : ViewModel() {


    private val newsEvent_ = MutableLiveData<NewsEvent>()
    var newsAPI:NewsAPI? = null
    var news:News? = null





    val newsEvent: LiveData<NewsEvent>
        get() = newsEvent_


    init {
        newsAPI = RetrofitInstance.getRetrofitInstance().create(NewsAPI::class.java)
        news = NewsImplementation(newsAPI!!)


    }


    sealed class NewsEvent {
        class Success(val blogs:NewsResponse):NewsEvent()
        class SuccessUpdated(val blogs:NewsResponse):NewsEvent()

        class SuccessLikeAdded(val newsLikeCommentAddedResponse: NewsLikeCommentAddedResponse):NewsEvent()
        class SuccessGetComments(val newsLikeCommentAddedResponse: List<NewsLikeCommentAddedResponse>):NewsEvent()
        class SuccessBlogSubscribe(val blogs:SubscribeResponse):NewsEvent()



        class SuccessGetReplyComments(val newsLikeCommentAddedResponse: List<NestedCommentResponse>):NewsEvent()

        class SuccessAddReplyComment(val newsLikeCommentAddedResponse: NestedCommentResponse):NewsEvent()
        class SuccessDeleteComment(val newsLikeCommentAddedResponse: DeleteCommentResponse):NewsEvent()
        class SuccessAddView(val newsAddViewResponse: NewsAddViewResponse):NewsEvent()


        class Failure(val message: String?) : NewsEvent()
        class SuccessBlogStatusSubscribe(val data: SubscribeStatusResponse) : NewsViewModel.NewsEvent() {

        }

        object Loading : NewsEvent()
        object Empty : NewsEvent()



    }


    fun addViewOnBlog(userID: String?, blogID: String?){
        val data = HashMap<String,String>()
        data.put("user_id",userID!!)
        data.put("blog_id",blogID!!)

        viewModelScope.launch {
            newsEvent_.value = NewsEvent.Loading

            val response = news!!.addViewOnBlog(data)

            when (response) {

                is Resource.Success -> {


                    newsEvent_.value = NewsEvent.SuccessAddView(response.data!!)

                }

                is Resource.Error -> {
                    newsEvent_.value = NewsEvent.Failure("Server error occured"!!)


                }


            }


        }



    }


    fun getAllBlogs(userID:String?) {

        val data = HashMap<String,String>()
        data.put("user_id",userID!!)

        viewModelScope.launch {
            newsEvent_.value = NewsEvent.Loading

            val response = news!!.showBlog(data)

            when (response) {

                is Resource.Success -> {


                    newsEvent_.value = NewsEvent.Success(response.data!!)

                }

                is Resource.Error -> {
                    newsEvent_.value = NewsEvent.Failure("Server error occured"!!)


                }


            }


        }


    }



    fun deleteCommentOnBlog(userID: String?,commentID: String?){

        val data = HashMap<String,String>()
        data.put("user_id",userID!!)
        data.put("id",commentID!!)

        viewModelScope.launch {
            newsEvent_.value = NewsEvent.Loading

            val response = news!!.deleteCommentOnBlog(data)

            when (response) {

                is Resource.Success -> {


                    newsEvent_.value = NewsEvent.SuccessDeleteComment(response.data!!)

                }

                is Resource.Error -> {
                    newsEvent_.value = NewsEvent.Failure("Server error occured"!!)


                }


            }


        }
    }



    fun getAllReplyComments(userID:String?, commentID:String?) {

        val data = HashMap<String,String>()
        data.put("user_id",userID!!)
        data.put("comment_id",commentID!!)

        viewModelScope.launch {
            newsEvent_.value = NewsEvent.Loading

            val response = news!!.showRepliesOnCommentOnBlog(data)

            when (response) {

                is Resource.Success -> {


                    newsEvent_.value = NewsEvent.SuccessGetReplyComments(response.data!!)

                }

                is Resource.Error -> {
                    newsEvent_.value = NewsEvent.Failure("Server error occured"!!)


                }


            }


        }


    }

    fun addReplyOnComment(userID:String?, commentID:String?, comment: String?) {

        val data = HashMap<String,String>()
        data.put("user_id",userID!!)
        data.put("comment_id",commentID!!)
        data.put("comment",comment!!)

        viewModelScope.launch {
            newsEvent_.value = NewsEvent.Loading

            val response = news!!.addReplyOnCommentOnBlog(data)

            when (response) {

                is Resource.Success -> {


                    newsEvent_.value = NewsEvent.SuccessAddReplyComment(response.data!!)

                }

                is Resource.Error -> {
                    newsEvent_.value = NewsEvent.Failure("Server error occured"!!)


                }


            }


        }


    }

    fun getAllBlogsLatest(userID:String?) {

        val data = HashMap<String,String>()
        data.put("user_id",userID!!)

        viewModelScope.launch {
            newsEvent_.value = NewsEvent.Loading

            val response = news!!.showBlog(data)

            when (response) {

                is Resource.Success -> {


                    newsEvent_.value = NewsEvent.SuccessUpdated(response.data!!)

                }

                is Resource.Error -> {
                    newsEvent_.value = NewsEvent.Failure("Server error occured"!!)


                }


            }


        }


    }
    fun addSubscribeOnBlogs(userID:String?, email:String?) {

        val data = HashMap<String,String>()
        data.put("user_id",userID!!)
        data.put("email",email!!)

        viewModelScope.launch {
            newsEvent_.value = NewsEvent.Loading

            val response = news!!.subscribeForBlog(data)

            when (response) {

                is Resource.Success -> {


                    newsEvent_.value = NewsEvent.SuccessBlogSubscribe(response.data!!)

                }

                is Resource.Error -> {
                    newsEvent_.value = NewsEvent.Failure("Server error occured"!!)


                }


            }


        }


    }

    fun getSubscribeStatus(userID:String?) {

        val data = HashMap<String,String>()
        data.put("user_id",userID!!)

        viewModelScope.launch {
            newsEvent_.value = NewsEvent.Loading

            val response = news!!.getSubscribeStatus(data)

            when (response) {

                is Resource.Success -> {


                    newsEvent_.value = NewsEvent.SuccessBlogStatusSubscribe(response.data!!)

                }

                is Resource.Error -> {
                    newsEvent_.value = NewsEvent.Failure("Server error occured"!!)


                }


            }


        }


    }
    fun addLikeOnPost(userID: String?, blogID:String?) {

        val map = HashMap<String,String>()
        map.put("user_id",userID!!)
        map.put("blog_id",blogID!!)


        viewModelScope.launch {

            val response = news!!.addLikeOnBlog(map)


            when (response) {

                is Resource.Success -> {


                    newsEvent_.value = NewsEvent.SuccessLikeAdded(response.data!!)
                    getAllBlogsLatest(userID)

                }

                is Resource.Error -> {
                    newsEvent_.value = NewsEvent.Failure("Server error occured"!!)


                }


            }


        }


    }


    fun addCommentOnPost(userID: String?, blogID:String?, comment:String?) {

        val map = HashMap<String,String>()
        map.put("user_id",userID!!)
        map.put("blog_id",blogID!!)
        map.put("comment",comment!!)


        viewModelScope.launch {
            newsEvent_.value = NewsEvent.Loading

            val response = news!!.addCommentOnBlog(map)


            when (response) {

                is Resource.Success -> {


                    newsEvent_.value = NewsEvent.SuccessLikeAdded(response.data!!)
                    getCommentsForBLog(userID,blogID)

                }

                is Resource.Error -> {
                    newsEvent_.value = NewsEvent.Failure("Server error occured"!!)


                }


            }


        }


    }


    fun getCommentsForBLog(userID: String?, blogID:String?) {

        val map = HashMap<String,String>()
        map.put("user_id",userID!!)
        map.put("blog_id",blogID!!)


        viewModelScope.launch {
            newsEvent_.value = NewsEvent.Loading

            val response = news!!.getCommentsForBLog(map)


            when (response) {

                is Resource.Success -> {


                    newsEvent_.value = NewsEvent.SuccessGetComments(response.data!!)

                }

                is Resource.Error -> {
                    newsEvent_.value = NewsEvent.Failure("Server error occured"!!)


                }


            }


        }


    }


}