package com.maestro.tdbizz.ui.newsletter.adapter

import com.google.gson.annotations.SerializedName



data class DeleteCommentResponse(
    @SerializedName("message")
    val message: String

)