package com.maestro.tdbizz.signup


import androidx.annotation.Nullable
import com.google.gson.annotations.SerializedName

data class VerifyOtpResponse(
    @SerializedName("data")
    val `data`: List<SignUpResponse>,
    @SerializedName("message")
    val message: String,
    @SerializedName("result")
    val result: Boolean,
    @SerializedName("isVendor")
    @Nullable
    val isVendor: String?

)