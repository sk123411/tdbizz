package com.maestro.tdbizz.ui.product.data

import com.maestro.tdbizz.ui.home.ProductResponse
import com.maestro.tdbizz.ui.product.model.ProductAddedResponse
import retrofit2.Response
import retrofit2.http.*

interface ProductAPI {


    @POST("api/process.php")
    @FormUrlEncoded
    suspend fun getProducts(@Query("action") action: String?,@FieldMap data:HashMap<String,String>): Response<ProductResponse>


    @POST("api/process.php")
    @FormUrlEncoded
    suspend fun addProductToWishlist(@Query("action") action: String?,@FieldMap data:HashMap<String,String>): Response<ProductAddedResponse>


    @POST("api/process.php")
    @FormUrlEncoded
    suspend fun showWishlist(@Query("action") action: String?,@FieldMap data:HashMap<String,String>): Response<ProductResponse>



}