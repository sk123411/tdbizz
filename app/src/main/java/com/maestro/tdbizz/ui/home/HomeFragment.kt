package com.maestro.tdbizz.ui.home

import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import androidx.navigation.Navigation
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.maestro.tdbizz.ui.product.ProductAdapter
import com.maestro.tdbizz.R
import com.maestro.tdbizz.databinding.FragmentHomeBinding
import com.maestro.tdbizz.helper.Constant
import com.maestro.tdbizz.ui.category.CategoryResponseItem
import com.maestro.tdbizz.ui.category.CategorySubcategoryAdapter
import com.maestro.tdbizz.ui.home.data.HomeViewModel
import com.maestro.tdbizz.ui.product.model.Slider
import com.maestro.tdbizz.ui.product.slider.SliderAdapter
import com.maestro.tdbizz.ui.vendor.VendorAdapter
import com.maestro.tdbizz.ui.vendor.data.VendorViewModel
import com.smarteist.autoimageslider.IndicatorView.animation.type.IndicatorAnimationType
import com.smarteist.autoimageslider.SliderAnimations
import io.paperdb.Paper

class HomeFragment : Fragment() {

    private lateinit var binding: FragmentHomeBinding
    private val homeViewModel: HomeViewModel by viewModels()
    private val vendorViewModel: VendorViewModel by viewModels()







    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {


        binding = FragmentHomeBinding.inflate(layoutInflater)

        homeViewModel.getCategories()
        homeViewModel.getProducts()
        homeViewModel.getFeatureProducts()
        vendorViewModel.getAllVendors()



        val toolbar = requireActivity().findViewById<androidx.appcompat.widget.Toolbar>(R.id.toolbar)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            toolbar.setTitle("Tdbizz")
        }






        binding.fhNews.setOnClickListener {


            Navigation.findNavController(requireView()).navigate(R.id.newsFragment)

        }
        binding.fhOffers.setOnClickListener {


            Navigation.findNavController(requireView()).navigate(R.id.offersFragment)

        }



        lifecycleScope.launchWhenStarted {


            vendorViewModel.vendorEvent.observe(viewLifecycleOwner, Observer {

                when (it) {


                    is VendorViewModel.HomeEvent.SuccessVendors -> {

                        activity?.findViewById<ProgressBar>(R.id.loadingBar)?.isVisible = false

                        if (it.vendors.isNotEmpty()) {


                            binding.vendorsList.apply {


                                layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL,false)
                                adapter =
                                    VendorAdapter(it.vendors)


                            }
                        }


                    }



                    is VendorViewModel.HomeEvent.Loading -> {
                        activity?.findViewById<ProgressBar>(R.id.loadingBar)?.isVisible = true


                    }
                }


            })
















            homeViewModel.homeEvent.observe(viewLifecycleOwner, Observer {




                when(it){


                    is HomeViewModel.HomeEvent.SuccessCategories -> {
                        activity?.findViewById<ProgressBar>(R.id.loadingBar)?.isVisible = false



                        binding.categoryShimmer.stopShimmerAnimation()
                        binding.categoryShimmer.visibility = View.GONE


                        binding.categoriesList.apply {


                            layoutManager = LinearLayoutManager(context,LinearLayoutManager.HORIZONTAL,false)
                            adapter =
                                CategorySubcategoryAdapter<CategoryResponseItem>(it.categories)

                        }


                    }

                    is HomeViewModel.HomeEvent.Loading -> {


                        activity?.findViewById<ProgressBar>(R.id.loadingBar)?.isVisible = true

                    }
                    is HomeViewModel.HomeEvent.SuccessFeatureProducts -> {


                        activity?.findViewById<ProgressBar>(R.id.loadingBar)?.isVisible = false

                        setupSlider(it.products)

                    }

                    is HomeViewModel.HomeEvent.SuccessProducts -> {

                        activity?.findViewById<ProgressBar>(R.id.loadingBar)?.isVisible = false


                        binding.productShimmer.stopShimmerAnimation()
                        binding.productShimmer.visibility = View.GONE

                        Paper.init(context)
                        Paper.book().write(Constant.PRODUCTS,it.products)



                        binding.productsList.apply {


                            layoutManager = LinearLayoutManager(context,LinearLayoutManager.HORIZONTAL,false)
                            adapter =
                                ProductAdapter(it.products)
                        }




                    }

                }




            })


        }



















        return binding.root
    }



    fun setupSlider(productResponseItem: List<ProductResponseItem>){
        val  sliderAdapter = SliderAdapter(requireContext())
        for (data in productResponseItem){
            sliderAdapter.addItem(
                Slider(
                    data.images.get(0).id,
                    data.productID,
                    "${Constant.BASE_URL_IMAGE}${data.images.get(0).image}"
                )
            )

        }

        binding.pdImageSlider.setSliderAdapter(sliderAdapter)
        binding.pdImageSlider.setIndicatorAnimation(IndicatorAnimationType.WORM);

        binding.pdImageSlider.isAutoCycle = true
        binding.pdImageSlider.scrollTimeInMillis=3000
        binding.pdImageSlider.setSliderTransformAnimation(SliderAnimations.ANTICLOCKSPINTRANSFORMATION);
        binding.pdImageSlider.startAutoCycle();

    }


    override fun onResume() {
        super.onResume()

        binding.categoryShimmer.startShimmerAnimation()
        binding.productShimmer.startShimmerAnimation()


    }
}