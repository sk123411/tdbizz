package com.maestro.tdbizz.ui.product.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class Slider(
    @SerializedName("id")
    @Expose
    val id:String?,
    @SerializedName("product_id")
    @Expose
    val product_id:String?,
    @SerializedName("image")
    @Expose
    val image:String?)
