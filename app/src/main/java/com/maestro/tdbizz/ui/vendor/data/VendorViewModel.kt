package com.maestro.tdbizz.ui.vendor.data

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.maestro.tdbizz.helper.Resource
import com.maestro.tdbizz.retrofit.RetrofitInstance
import com.maestro.tdbizz.ui.category.CategoryResponse
import com.maestro.tdbizz.ui.category.SubCategoryResponseItem
import com.maestro.tdbizz.ui.home.ProductResponse
import com.maestro.tdbizz.ui.home.ProductResponseItem
import com.maestro.tdbizz.ui.vendor.model.VendorDetailResponse
import com.maestro.tdbizz.ui.vendor.model.VendorResponse

import kotlinx.coroutines.launch
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.File

class VendorViewModel : ViewModel() {


    private val vendorEvent_ = MutableLiveData<HomeEvent>()
    var vendorsAPI: VendorAPI? = null
    var vendor: Vendor? = null



    private val imageFile_ = MutableLiveData<File>()



    val imageFile: LiveData<File>
        get() = imageFile_


    val vendorEvent: LiveData<HomeEvent>
        get() = vendorEvent_


    init {
        vendorsAPI = RetrofitInstance.getRetrofitInstance().create(VendorAPI::class.java)
        vendor = VendorImplementation(vendorsAPI!!)


    }


    sealed class HomeEvent {
        class SuccessVendorDetail(val vendors: VendorDetailResponse) : HomeEvent()
        class SuccessVendorProducts(val productResponse: ProductResponse) : HomeEvent()

        class SuccessVendors(val vendors: List<VendorResponse>) : HomeEvent()
        class SuccessProductAdded(val productResponseItem: ProductResponseItem) : HomeEvent()
        class Failure(val message: String?) : HomeEvent()
        object Loading : HomeEvent()
        object Empty : HomeEvent()



    }


    fun getAllVendors() {


        viewModelScope.launch {
            vendorEvent_.value = HomeEvent.Loading

            val response = vendor!!.getAllVendors()

            when (response) {

                is Resource.Success -> {


                    vendorEvent_.value = HomeEvent.SuccessVendors(response.data!!)

                }

                is Resource.Error -> {
                    vendorEvent_.value = HomeEvent.Failure("Server error occured"!!)


                }


            }


        }


    }



    fun getVendorDetails(vendorID: String?) {

        val map = HashMap<String,String>()
        map.put("vendor_id",vendorID!!)


        viewModelScope.launch {
            vendorEvent_.value = HomeEvent.Loading

            val response = vendor!!.getVendorDetails(map)


            when (response) {

                is Resource.Success -> {


                    vendorEvent_.value = HomeEvent.SuccessVendorDetail(response.data!!)

                }

                is Resource.Error -> {
                    vendorEvent_.value = HomeEvent.Failure("Server error occured"!!)


                }


            }


        }


    }


    fun changeVendorDetails(vendorID: String?, name: String?,storeName: String?) {

        val map = HashMap<String,String>()
        map.put("id",vendorID!!)
        map.put("name",name!!)
        map.put("store_name",storeName!!)


        viewModelScope.launch {
            vendorEvent_.value = HomeEvent.Loading

            val response = vendor!!.changeVendorDetails(map)


            when (response) {

                is Resource.Success -> {


                    vendorEvent_.value = HomeEvent.SuccessVendorDetail(response.data!!)

                }

                is Resource.Error -> {
                    vendorEvent_.value = HomeEvent.Failure("Server error occured"!!)


                }


            }


        }


    }



    fun changeVendorStoreImage(vendorID: String?,file:File?) {


        val storeImage: RequestBody =  RequestBody.create(
            "image/jpg".toMediaTypeOrNull(),
            file!!)

        val storeImageBody: MultipartBody.Part =
            MultipartBody.Part.createFormData(
                "image",
                file.getName(), storeImage)



        val nameBody = getMultiPartFormRequestBody(vendorID)





        viewModelScope.launch {
            vendorEvent_.value = HomeEvent.Loading

            val response = vendor!!.changeVendorStoreImage(storeImageBody,nameBody)


            when (response) {

                is Resource.Success -> {


                    vendorEvent_.value = HomeEvent.SuccessVendorDetail(response.data!!)

                }

                is Resource.Error -> {
                    vendorEvent_.value = HomeEvent.Failure("Server error occured"!!)


                }


            }


        }


    }
    fun getVendorProducts(vendorID: String?) {

        val map = HashMap<String,String>()
        map.put("vendor_id",vendorID!!)

        viewModelScope.launch {
            vendorEvent_.value = HomeEvent.Loading

            val response = vendor!!.getVendorProducts(map)

            when (response) {

                is Resource.Success -> {


                    vendorEvent_.value = HomeEvent.SuccessVendorProducts(response.data!!)

                }

                is Resource.Error -> {
                    vendorEvent_.value = HomeEvent.Failure("Server error occured"!!)


                }


            }


        }


    }







    fun addProduct(file:File?,name:String?,description:String?,price:String?,size:String?,color:String?,brand:String?,category_id:String?,subcategory_id:String?,vendorID:String?) {

        val storeImage: RequestBody =  RequestBody.create(
            "image/jpg".toMediaTypeOrNull(),
            file!!)

        val storeImageBody: MultipartBody.Part =
            MultipartBody.Part.createFormData(
                "image[]",
                file.getName(), storeImage)



        val nameBody = getMultiPartFormRequestBody(name)
        val descBody = getMultiPartFormRequestBody(description)
        val priceBody = getMultiPartFormRequestBody(price)
        val sizeBody = getMultiPartFormRequestBody(size)
        val colorBody = getMultiPartFormRequestBody(color)
        val brandBody = getMultiPartFormRequestBody(brand)
        val categoryIDBody = getMultiPartFormRequestBody(category_id)
        val subCategoryIDBody = getMultiPartFormRequestBody(subcategory_id)
        val arrivalStatusIDBody = getMultiPartFormRequestBody("arrival_status")
        val vendorIDBody = getMultiPartFormRequestBody(vendorID)

        viewModelScope.launch {
            vendorEvent_.value = HomeEvent.Loading



            val response = vendor!!.addProduct(storeImageBody,nameBody,descBody,priceBody,brandBody,sizeBody,colorBody,
                categoryIDBody,subCategoryIDBody,arrivalStatusIDBody,vendorIDBody)


            when (response) {

                is Resource.Success -> {


                    vendorEvent_.value = HomeEvent.SuccessProductAdded(response.data!!)

                }

                is Resource.Error -> {
                    vendorEvent_.value = HomeEvent.Failure("Server error occured"!!)


                }


            }


        }


    }

    fun getMultiPartFormRequestBody(tag:String?): RequestBody {
        return RequestBody.create(MultipartBody.FORM, tag!!)

    }

    fun sendImageFile(file: File) {

        imageFile_.value = file
    }



}