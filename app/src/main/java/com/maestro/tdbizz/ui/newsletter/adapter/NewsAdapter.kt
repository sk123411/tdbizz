package com.maestro.tdbizz.ui.newsletter.adapter

import android.content.Context
import android.graphics.Bitmap
import android.media.MediaMetadataRetriever
import android.media.MediaPlayer
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.google.android.exoplayer2.MediaItem
import com.google.android.exoplayer2.SimpleExoPlayer
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.button.MaterialButton
import com.maestro.tdbizz.R
import com.maestro.tdbizz.databinding.PostDetailLytBinding
import com.maestro.tdbizz.databinding.PostItemBinding
import com.maestro.tdbizz.helper.Constant
import com.maestro.tdbizz.ui.newsletter.LifeCycleMethodCalledInterface
import com.maestro.tdbizz.ui.newsletter.NewsFragment
import com.maestro.tdbizz.ui.newsletter.NewsResponseItem
import com.maestro.tdbizz.ui.newsletter.NewsViewModel
import java.util.*


class NewsAdapter(
    var list: List<NewsResponseItem>,
    val newsFeedFragment: NewsFragment,
    val newsViewModel: NewsViewModel
) : RecyclerView.Adapter<NewsAdapter.ViewHolder>() {



    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val binding = PostItemBinding.bind(view)
        var player: SimpleExoPlayer? = null
        private var playWhenReady = true
        private var currentWindow = 0
        private var playbackPosition: Long = 0






        fun bindData(
            newsResponseItem: NewsResponseItem,
            newsFeedFragment: NewsFragment,
            newsViewModel: NewsViewModel

            ) {
            binding.postUserName.text = newsResponseItem.author
            binding.postTilte.text = newsResponseItem.title



           binding.postUserImage.setImageResource(R.drawable.rock)



            if (newsResponseItem.status.equals("0")){

                binding.postTilte.text = "Survey-> "+ newsResponseItem.title
                binding.postTilte.setTextColor(itemView.resources.getColor(R.color.teal_700))
                binding.imageViewRoot.visibility = View.GONE
                if (newsResponseItem.title.isEmpty()){
                    binding.postTilte.text = "Lorem sorem thompson get added"
                }

            }

            if (newsResponseItem.status.equals("1")){
                binding.imageViewRoot.visibility = View.VISIBLE
                binding.mViewViewRoot.visibility = View.GONE
                if (newsResponseItem.title.isEmpty()){
                    binding.postTilte.text = "Lorem sorem thompson get added"
                }
                binding.postTilte.text = newsResponseItem.title
                binding.postImage.visibility = View.VISIBLE
                Glide.with(itemView).load(Constant.BASE_URL_IMAGE + newsResponseItem.image_video).placeholder(R.drawable.logo)
                    .into(binding.postImage)

            }


            if (newsResponseItem.status.equals("3")){

                binding.postTilte.text = newsResponseItem.title

                if (newsResponseItem.title.isEmpty()){
                    binding.postTilte.text = "Lorem sorem thompson get added"
                }

                binding.imageViewRoot.visibility = View.GONE


            }

            if (newsResponseItem.status.equals("2")){
                binding.imageViewRoot.visibility = View.VISIBLE
                if (newsResponseItem.title.isEmpty()){
                    binding.postTilte.text = "Lorem sorem thompson get added"
                }
                binding.postImage.visibility = View.GONE
                binding.mViewViewRoot.visibility = View.VISIBLE
                val url = newsResponseItem.image_video // your URL here


                Glide.with(itemView)
                    .load("https://res.cloudinary.com/dlmt4hsgw/video/upload/v1557557542/3_37.mp4")
                    .centerCrop()
                    .into(binding.thumbnailVideo);


                var mediaPlayer: MediaPlayer? = null
                try {


                    binding.playButton.setOnClickListener {
                        binding.playButton.visibility = View.GONE
                        binding.thumbnailVideo.visibility = View.GONE

                        initializePlayer(itemView.context, newsResponseItem,binding)

                    }
                    if (player!!.isPlaying){

                        binding.playButton.visibility = View.GONE
                    }else {
                        binding.playButton.visibility = View.VISIBLE
                    }


                } catch (e: Exception) {

                    Log.d("ERRRRRRRRRRRRRR", "" + e.toString())


                }


            }

//            binding.likeCountText.text = newsResponseItem.like_count.toString()
//            binding.commentCountText.text = newsResponseItem.comment_count.toString()

            binding.likeTextView.text = newsResponseItem.like_count.toString() +" "+ "Likes"
            binding.commentTextView.text = newsResponseItem.comment_count.toString() +" "+ "Comments"







            if (newsResponseItem.likeStatus == 1) {

                binding.postLikeButton.setImageResource(R.drawable.heart_full)

            } else {
                binding.postLikeButton.setImageResource(R.drawable.heart_gray_hollow)

            }


            //Set image with picasso


            binding.postCommentButton.setOnClickListener {
                //      newsViewModel.getCommentsForBLog(new.id)
                openPostDetail(newsResponseItem, newsFeedFragment, newsViewModel,newsFeedFragment.requireActivity())
            }

            binding.viewCountText.text = newsResponseItem.view_count +" Views"


            binding.postLikeButton.setOnClickListener {

                newsViewModel.addLikeOnPost(
                    Constant.getUserID(it.context).id,
                    newsResponseItem.id
                )
                if (newsResponseItem.likeStatus == 1) {

                    if (newsResponseItem.like_count.toInt() > 1) {
                        binding.likeCountText.text = (newsResponseItem.like_count - 1).toString()
                    }
                    binding.postLikeButton.setImageResource(R.drawable.heart_gray_hollow)

                } else {

                    binding.likeCountText.text = (newsResponseItem.like_count + 1).toString()

                    binding.postLikeButton.setImageResource(R.drawable.heart_full)
                }
            }




        }


        private fun openPostDetail(
            newsResponseItem: NewsResponseItem,
            newsFeedFragment: NewsFragment,
            newsViewModel: NewsViewModel,
            activity:FragmentActivity
        ) {


            val bottomSheetDialog = BottomSheetDialog(newsFeedFragment.requireActivity())
            val binding =
                PostDetailLytBinding.inflate(LayoutInflater.from(newsFeedFragment.requireContext()))
            bottomSheetDialog.setContentView(binding.root)
//            binding.postUserImage.setImageResource(R.drawable.rock)

            bottomSheetDialog.show()
            newsViewModel.getCommentsForBLog(
                Constant.getUserID(itemView.context).id,
                newsResponseItem.id
            )
            newsViewModel.addViewOnBlog(
                Constant.getUserID(itemView.context).id,
                newsResponseItem.id
            )

            newsViewModel.newsEvent.observe(
                newsFeedFragment.viewLifecycleOwner,
                Observer {

                    when (it) {

                        is NewsViewModel.NewsEvent.SuccessGetComments -> {


                            Collections.reverse(it.newsLikeCommentAddedResponse)

                            binding.commentList.apply {

                                layoutManager = LinearLayoutManager(context)
                                adapter = NewsCommentAdapter(it.newsLikeCommentAddedResponse,newsViewModel,newsFeedFragment, activity)

                            }


                        }

                        is NewsViewModel.NewsEvent.SuccessAddView -> {

                            Log.d("VIEW_ADDED", "COOOL")

                        }

                        is NewsViewModel.NewsEvent.Failure -> {

                            Toast.makeText(
                                newsFeedFragment.requireActivity(),
                                it.message,
                                Toast.LENGTH_SHORT
                            ).show()

                        }

                        is NewsViewModel.NewsEvent.Loading -> {




                        }





                        is NewsViewModel.NewsEvent.SuccessLikeAdded -> {
                            Toast.makeText(
                                newsFeedFragment.requireActivity(),
                                it.newsLikeCommentAddedResponse.result,
                                Toast.LENGTH_SHORT
                            ).show()
                            if (it.newsLikeCommentAddedResponse.comment != null) {
                                Toast.makeText(
                                    newsFeedFragment.requireActivity(),
                                    "Comment sent",
                                    Toast.LENGTH_SHORT
                                ).show()
                            }
                        }
                    }
                })



            binding.sendMessageButtton.setOnClickListener {


                if (!binding.messageEdit.text.toString().equals("")) {

//                    var commentCount: Int? = binding.commentCountText.text.toString().toInt()
//                    var increasedCommentCount = commentCount!!.plus(1)



//                    binding.commentCountText.text = increasedCommentCount.toString()
                    newsViewModel.addCommentOnPost(
                        userID = Constant.getUserID(it.context).id,
                        blogID = newsResponseItem.id,
                        comment = binding.messageEdit.text.toString()
                    )

                    binding.messageEdit.setText("")
                }


            }


//            binding.postLikeButton.setOnClickListener {
//
//                newsViewModel.addLikeOnPost(
//                    Constant.getUserID(it.context).id,
//                    newsResponseItem.id
//                )
//
//                if (newsResponseItem.likeStatus == 1) {
//
//                    if (newsResponseItem.like_count.toInt() > 1) {
//                        binding.likeCountText.text =
//                            (newsResponseItem.like_count - 1).toString()
//                    }
//                    binding.postLikeButton.setImageResource(R.drawable.heart)
//
//                } else {
//
//
//                    binding.likeCountText.text = (newsResponseItem.like_count + 1).toString()
//
//                    binding.postLikeButton.setImageResource(R.drawable.heart_full)
//                }
//            }

            bottomSheetDialog.setOnDismissListener {

                newsViewModel.getAllBlogsLatest(Constant.getUserID(itemView.context).id)

//                releasePlayer()


            }





        }





        fun initializePlayer(context: Context, newsResponseItem: NewsResponseItem,binding: PostItemBinding) {
            player = SimpleExoPlayer.Builder(context).build()

            val mediaItem: MediaItem = MediaItem.fromUri("https://res.cloudinary.com/dlmt4hsgw/video/upload/v1557556254/3_35.mp4")
            player!!.setMediaItem(mediaItem)
            player!!.setPlayWhenReady(playWhenReady);
            player!!.seekTo(currentWindow, playbackPosition);
            player!!.prepare();

            binding.mViewView.setPlayer(player)

            NewsFragment.lifeCycleMethodCalledInterface = object :LifeCycleMethodCalledInterface{
                override fun onPause() {
                    releasePlayer()
                }

            }


        }


        fun releasePlayer()
        {

            if (player != null) {

                player!!.stop()
                playWhenReady = player!!.getPlayWhenReady();
                playbackPosition = player!!.getCurrentPosition();
                currentWindow = player!!.getCurrentWindowIndex();

                player!!.release();
                player = null;
            }

        }


        @Throws(Throwable::class)
        fun retriveVideoFrameFromVideo(videoPath: String?): Bitmap? {
            var bitmap: Bitmap? = null
            var mediaMetadataRetriever: MediaMetadataRetriever? = null
            try {
                mediaMetadataRetriever = MediaMetadataRetriever()
                mediaMetadataRetriever.setDataSource(videoPath, HashMap())
                //   mediaMetadataRetriever.setDataSource(videoPath);
                bitmap = mediaMetadataRetriever.frameAtTime
            } catch (e: java.lang.Exception) {
                e.printStackTrace()
                throw Throwable("Exception in retriveVideoFrameFromVideo(String videoPath)" + e.message)
            } finally {
                mediaMetadataRetriever?.release()
            }
            return bitmap
        }

    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        val view = LayoutInflater.from(parent.context).inflate(
            R.layout.post_item,
            parent, false
        )

        return ViewHolder(view)

    }


    fun refreshList(newsResponseItem: List<NewsResponseItem>){
        this.list = newsResponseItem;
        notifyDataSetChanged()

    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        holder.bindData(list.get(position), newsFeedFragment, newsViewModel)





    }





}